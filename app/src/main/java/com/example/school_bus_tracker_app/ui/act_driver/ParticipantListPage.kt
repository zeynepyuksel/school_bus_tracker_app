package com.example.school_bus_tracker_app.ui.act_driver
import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.school_bus_tracker_app.R
import com.example.school_bus_tracker_app.databinding.RecylerviewLayoutBinding
import com.example.school_bus_tracker_app.model.StudentModel
import com.example.school_bus_tracker_app.notification.NotificationPage
import com.example.school_bus_tracker_app.ui.act_driver.adapter.ParticipantAdapter
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.base_alert_dialog_layout.view.*

class ParticipantListPage(a:String)  : Fragment() {
    private val serviceId : String = a
    private var uid : String? = null
    private var markerList = HashMap<String, Marker>()
    val participantList : MutableList<StudentModel> = mutableListOf()
    private val participantAdapter = ParticipantAdapter(mutableListOf(),serviceId)
    private val rootRef = FirebaseDatabase.getInstance().reference
    private val notification = NotificationPage()
    private var onClickSeeConnectors: ParticipantAdapter.ItemClickListener? =
        object : ParticipantAdapter.ItemClickListener {
            override fun onClickSeeConnectors(student: StudentModel) {
                createAlertDialog(student)
            }
        }
    private var onClickGetOnSeeConnectors: ParticipantAdapter.ItemClickListener? =
        object : ParticipantAdapter.ItemClickListener {
            @SuppressLint("ShowToast")
            override fun onClickSeeConnectors(student: StudentModel) {
                val message = student.name + " got on the service."
                sendMessage(student, message, "Got On")
            }
        }
    private var onClickGetOffSeeConnectors: ParticipantAdapter.ItemClickListener? =
        object : ParticipantAdapter.ItemClickListener {
            override fun onClickSeeConnectors(student: StudentModel) {
                val message = student.name + " got off the service."
                sendMessage(student, message, "Got Off")
            }
        }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding : RecylerviewLayoutBinding = DataBindingUtil.inflate(inflater, R.layout.recylerview_layout, container, false);
        binding.recylerView.layoutManager = LinearLayoutManager(activity,LinearLayoutManager.VERTICAL,false)
        uid = FirebaseAuth.getInstance().currentUser?.uid
        participantAdapter.setOnSeeConnectorsClickListener(onClickSeeConnectors)
        participantAdapter.setOnSeeConnectorsGetOnClickListener(onClickGetOnSeeConnectors)
        participantAdapter.setOnSeeConnectorsGetOffClickListener(onClickGetOffSeeConnectors)

        binding.recylerView.adapter = participantAdapter
        getStudentIds(serviceId)

        return binding.root
    }
    fun setMarker(markerList: HashMap<String, Marker>) {
        this.markerList = markerList
    }
    private fun sendMessage(student: StudentModel,message: String, status:String){
        rootRef.child(String.format("Services/%s", serviceId)).addListenerForSingleValueEvent(object:
            ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val drive = dataSnapshot.child("drive").value.toString()
                val where = dataSnapshot.child("where").value.toString()
                if(drive == "on"){
                    if (student.absent == "true"){
                        Toast.makeText(context,"Student won't come the service",Toast.LENGTH_SHORT).show()
                        markerList[student.userId]?.isVisible = false
                    }
                    else {
                        sendParentsMessage(message, student.userId)
                        val rootRef2 = FirebaseDatabase.getInstance().reference
                        rootRef2.child("Users").child(student.userId).child("status").setValue(status)
                        if(where == "goSchool" && status == "Got On"){
                            markerList[student.userId]?.isVisible = false
                        }
                        if(where == "goHome" && status == "Got Off"){
                            markerList[student.userId]?.isVisible = false
                        }
                    }

                }
                else{
                    Toast.makeText(context,"Service drive is off",Toast.LENGTH_SHORT).show()
                }
            }})
    }
    private fun sendParentsMessage(message : String,studentId:String){
        rootRef.child(String.format("Users/%s/Parents", studentId)).addListenerForSingleValueEvent(object:
            ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (snapshot in dataSnapshot.children) {
                    val parentId: String? = snapshot.getValue(String::class.java)
                    if (parentId != null) {
                        uid?.let { it1 -> notification.createNotification(it1,parentId,message,serviceId)}
                    }
                }
            }})
    }
    private fun deleteStudent(student : StudentModel){
        rootRef.child("Users").child(student.userId).child("myService").setValue(" ")
    }

    private fun getStudentIds(serviceId : String){
        val studentIdList = ArrayList<String>()
        rootRef.child(String.format("Services/%s/participants", serviceId)).addListenerForSingleValueEvent(object:
            ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (snapshot in dataSnapshot.children) {
                    val studentId: String? = snapshot.getValue(String::class.java)
                    if (studentId != null) {
                        studentIdList.add(studentId)
                    }
                }
                getStudentObjects(studentIdList)
            }})
    }
    fun getStudentObjects(studentIds : ArrayList<String>){
        for(id in studentIds){
            val student = StudentModel()
            rootRef.child(String.format("Users/%s", id)).addListenerForSingleValueEvent(object:
                ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    student.userId = dataSnapshot.child("userId").value.toString()
                    student.name = dataSnapshot.child("name").value.toString()
                    student.schoolName = dataSnapshot.child("schoolName").value.toString()
                    student.age = dataSnapshot.child("age").value.toString()
                    student.absent = dataSnapshot.child("absent").value.toString()
                    student.telephoneNumber =
                        dataSnapshot.child("telephoneNumber").value.toString()

                    participantList.add(student)
                    //participantAdapter.notifyDataSetChanged()
                    participantAdapter.setNewInstance(participantList)
                    participantAdapter.notifyDataSetChanged()
                }
            })
        }
    }
    fun createAlertDialog(student: StudentModel){
        val mDialogView = layoutInflater.inflate(R.layout.base_alert_dialog_layout,null)
        mDialogView.message.text = getString(R.string.deleteStudentServiceMessage)
        val mBuilder = context?.let {
            AlertDialog.Builder(it)
                .setView(mDialogView)
                .setCancelable(false)
        }
        val mAlertDialog = mBuilder?.show()
        mDialogView.alert_okey.setOnClickListener {
            participantAdapter.editList(student)
            deleteStudent(student)
            mAlertDialog?.dismiss()
        }
        mDialogView.alert_cancel.setOnClickListener {
            mAlertDialog?.dismiss()
        }
        mAlertDialog?.window?.setBackgroundDrawableResource(R.drawable.round_corner)
    }

}