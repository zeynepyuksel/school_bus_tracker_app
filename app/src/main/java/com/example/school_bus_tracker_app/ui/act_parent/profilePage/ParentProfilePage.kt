package com.example.school_bus_tracker_app.ui.act_parent.profilePage

import android.os.Bundle
import com.example.school_bus_tracker_app.R
import com.example.school_bus_tracker_app.core.BaseActivity
import com.example.school_bus_tracker_app.databinding.ProfilePageLayoutBinding

class ParentProfilePage : BaseActivity<ProfilePageLayoutBinding>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.profile_page_layout)

        binding.editPageBtn.setOnClickListener{
            updateUI2<ParentEditProfilePage>()
        }
        binding.backPageBtn.setOnClickListener {
            onBackPressed()
        }

    }
    override fun getLayout() = R.layout.profile_page_layout
}