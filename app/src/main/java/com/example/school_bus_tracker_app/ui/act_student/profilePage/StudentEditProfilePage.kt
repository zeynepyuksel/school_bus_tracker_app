package com.example.school_bus_tracker_app.ui.act_student.profilePage

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.Toast
import com.example.school_bus_tracker_app.R
import com.example.school_bus_tracker_app.core.BaseActivity
import com.example.school_bus_tracker_app.databinding.StudentEditProfilePageLayoutBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class StudentEditProfilePage : BaseActivity<StudentEditProfilePageLayoutBinding>() {
    private val rootRef = FirebaseDatabase.getInstance().reference
    private var uid = FirebaseAuth.getInstance().currentUser?.uid
    @SuppressLint("ShowToast")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.student_edit_profile_page_layout)
        showProfileInfo()

        binding.backPageBtn.setOnClickListener{
            onBackPressed()
        }
        binding.saveBtn.setOnClickListener {
            controlInfo()
            Toast.makeText(this, "Saved", Toast.LENGTH_LONG)
            updateUI<StudentProfilePage>()
        }

    }
    override fun getLayout() = R.layout.student_edit_profile_page_layout
    private fun controlInfo(){
        val name = binding.editStudentNameSurname.editText?.text.toString().trim()
        val telephoneNumber = binding.editStudentTelephoneNumber.editText?.text.toString().trim()
        val age = binding.editStudentAge.editText?.text.toString().trim()
        val schoolName = binding.editStudentSchoolName.editText?.text.toString().trim()
        changeInfo(name,"name")
        changeInfo(telephoneNumber,"telephoneNumber")
        changeInfo(age,"age")
        changeInfo(schoolName,"schoolName")
    }
    private fun showProfileInfo(){
        var uidRef = uid?.let { rootRef.child("Users").child(it) }
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                }
                else{
                    val name = dataSnapshot.child("name").value.toString()
                    val age = dataSnapshot.child("age").value.toString()
                    val telephoneNumber = dataSnapshot.child("telephoneNumber").value.toString()
                    val schoolName = dataSnapshot.child("schoolName").value.toString()
                    binding.editStudentNameSurname.hint = name
                    binding.editStudentAge.hint = age
                    binding.editStudentTelephoneNumber.hint = telephoneNumber
                    binding.editStudentSchoolName.hint = schoolName
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {}
        }
        uidRef?.addListenerForSingleValueEvent(eventListener)

    }
    private fun changeInfo(value:String,dbPath:String){
        if(value.isNotEmpty()){
            uid?.let { rootRef.child("Users").child(it)}?.child(dbPath)?.setValue(value)
        }
    }
}
