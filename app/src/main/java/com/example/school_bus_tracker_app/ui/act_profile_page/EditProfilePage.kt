package com.example.school_bus_tracker_app.ui.act_profile_page

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.Toast
import com.example.school_bus_tracker_app.R
import com.example.school_bus_tracker_app.core.BaseActivity
import com.example.school_bus_tracker_app.databinding.ProfileEditPageLayoutBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.create_service_dialog_layout.view.*

class EditProfilePage : BaseActivity<ProfileEditPageLayoutBinding>() {
    private val rootRef = FirebaseDatabase.getInstance().reference
    private var uid = FirebaseAuth.getInstance().currentUser?.uid
    @SuppressLint("ShowToast")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.profile_edit_page_layout)
        showProfileInfo()
        binding.backPageBtn.setOnClickListener{
            onBackPressed()
        }
        binding.saveBtn.setOnClickListener {
            controlInfo()
            Toast.makeText(this, "Saved",Toast.LENGTH_LONG)
            updateUI<ProfilePage>()
        }

    }
    override fun getLayout() = R.layout.profile_edit_page_layout
    private fun controlInfo(){
        val name = binding.editNameSurname.editText?.text.toString().trim()
        val telephoneNumber = binding.editTelephoneNumber.editText?.text.toString().trim()
        val age = binding.editAge.editText?.text.toString().trim()
        changeInfo(name,"name")
        changeInfo(telephoneNumber,"telephoneNumber")
        changeInfo(age,"age")
    }
    private fun showProfileInfo(){
        var uidRef = uid?.let { rootRef.child("Users").child(it) }
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                }
                else{
                    val userType = dataSnapshot.child("userType").value.toString()
                    val name:String = dataSnapshot.child("name").value.toString()
                    val age = dataSnapshot.child("age").value.toString()
                    val telephoneNumber = dataSnapshot.child("telephoneNumber").value.toString()
                    binding.editNameSurname.hint = name
                    binding.editAge.hint = age
                    binding.editTelephoneNumber.hint = telephoneNumber
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {}
        }
        uidRef?.addListenerForSingleValueEvent(eventListener)

    }
    private fun changeInfo(value:String,dbPath:String){
        if(value.isNotEmpty()){
            uid?.let { rootRef.child("Users").child(it)}?.child(dbPath)?.setValue(value)
        }
    }
}