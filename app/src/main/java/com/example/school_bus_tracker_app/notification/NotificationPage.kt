package com.example.school_bus_tracker_app.notification

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.school_bus_tracker_app.R
import com.example.school_bus_tracker_app.core.BaseActivity
import com.example.school_bus_tracker_app.databinding.NotifRecylerviewLayoutBinding
import com.example.school_bus_tracker_app.model.NotificationModel
import com.example.school_bus_tracker_app.model.StudentModel
import com.example.school_bus_tracker_app.ui.act_driver.adapter.WaitingParticipantAdapter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

class NotificationPage : BaseActivity<NotifRecylerviewLayoutBinding>(){
    val notificationList : MutableList<NotificationModel> = mutableListOf()
    private val notificationAdapter = NotificationAdapter(mutableListOf())
    private var mDatabaseReference: DatabaseReference? = null
    private var mDatabase: FirebaseDatabase? = null
    private var uid : String? = null
    private var onClickSeeConnectorsDelete: NotificationAdapter.ItemClickNotificationDeleteListener? =
        object : NotificationAdapter.ItemClickNotificationDeleteListener {
           override fun onClickSeeConnectors(notification: NotificationModel) {
                notificationAdapter.editList(notification)
           }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.notif_recylerview_layout)
        uid = FirebaseAuth.getInstance().currentUser?.uid
        binding.recylerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        notificationAdapter.setOnSeeConnectorsDeleteClickListener(onClickSeeConnectorsDelete)
        binding.recylerView.adapter = notificationAdapter
        notificationIds()

    }
    override fun getLayout() = R.layout.notif_recylerview_layout
    fun createNotification(from:String, to:String, text:String, serviceId:String){
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference.child("Notifications")
        val notificationDb = mDatabaseReference!!.push()
        val mDatabaseRef = mDatabase!!.reference.child("Users").child(from)
        val listener: ValueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val name = dataSnapshot.child("name").value.toString()
                notificationDb.child("fromName").setValue(name)
            }
            override fun onCancelled(databaseError: DatabaseError) {}
        }
        mDatabaseRef.addListenerForSingleValueEvent(listener)

        notificationDb.child("from").setValue(from)
        notificationDb.child("to").setValue(to)
        notificationDb.child("notificationId").setValue(notificationDb.key)
        notificationDb.child("text").setValue(text)
        notificationDb.child("serviceId").setValue(serviceId)
        notificationDb.child("gone").setValue("false")


        val rootRef = FirebaseDatabase.getInstance().reference
        var uidRef = rootRef.child("Users").child(to)
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                }
                else{
                    val notificationNumberStr = dataSnapshot.child("notificationNumber").value.toString()
                    var notificationNumber = notificationNumberStr.toInt()
                    uidRef = uidRef.child("Notifications")
                    uidRef.child(notificationNumber.toString()).setValue(notificationDb.key)
                    notificationNumber += 1
                    uidRef = rootRef.child("Users").child(to)
                    uidRef.child("notificationNumber").setValue(notificationNumber.toString())
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {}
        }
        uidRef.addListenerForSingleValueEvent(eventListener)

    }
    private fun notificationIds() {
        val notificationIdList = ArrayList<String>()
        val uid = FirebaseAuth.getInstance().currentUser!!.uid
        val rootRef = FirebaseDatabase.getInstance().reference
        rootRef.child(String.format("Users/%s/Notifications", uid)).addListenerForSingleValueEvent(object:
            ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (snapshot in dataSnapshot.children) {
                    val notificationId: String? = snapshot.getValue(String::class.java)
                    if (notificationId != null) {
                        notificationIdList.add(notificationId)
                    }
                }
                getServiceObjects(notificationIdList)
            }})
    }
    fun getServiceObjects(notificationsIds : ArrayList<String>) {
        val rootRef = FirebaseDatabase.getInstance().reference
        for (id in notificationsIds) {
            val notification = NotificationModel()
            rootRef.child(String.format("Notifications/%s", id)).addListenerForSingleValueEvent(object :
                ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    notification.from = dataSnapshot.child("from").value.toString()
                    notification.to = dataSnapshot.child("to").value.toString()
                    notification.fromName = dataSnapshot.child("fromName").value.toString()
                    notification.text = dataSnapshot.child("text").value.toString()
                    notification.notificationId =
                        dataSnapshot.child("notificationId").value.toString()
                    notificationList.add(notification)


                    if (id == notificationsIds[notificationsIds.size - 1]) {
                        notificationAdapter.setNewInstance(notificationList)
                        notificationAdapter.notifyDataSetChanged()
                    }
                }
            })
        }
    }


}