package com.example.school_bus_tracker_app.ui.act_parent

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.school_bus_tracker_app.R
import com.example.school_bus_tracker_app.core.BaseActivity
import com.example.school_bus_tracker_app.databinding.ChildrenListLayoutBinding
import com.example.school_bus_tracker_app.model.StudentModel
import com.example.school_bus_tracker_app.ui.act_parent.adapter.ChildrenAdapter
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.school_bus_tracker_app.ui.act_driver.ServiceDetailPage
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.base_alert_dialog_layout.view.*

private val rootRef = FirebaseDatabase.getInstance().reference


class ChildrenListPage : BaseActivity<ChildrenListLayoutBinding>() {
    val childrenList : MutableList<StudentModel> = mutableListOf()
    val uid = FirebaseAuth.getInstance().currentUser!!.uid
    val rootRef = FirebaseDatabase.getInstance().reference
    private val childrenAdapter = ChildrenAdapter(mutableListOf())
    private var onClickSeeConnectors: ChildrenAdapter.ItemClickListener? =
        object : ChildrenAdapter.ItemClickListener {
            override fun onClickSeeConnectors(student: StudentModel) {
                createAlertDialog(student)
            }
        }
    private var onClickSeeConnectorsCardView: ChildrenAdapter.ItemClickListener? =
        object : ChildrenAdapter.ItemClickListener {
            override fun onClickSeeConnectors(student: StudentModel) {
                Log.d("****",student.myService)
                if(student.myService != "null" && student.myService != ""  && student.myService != " ") {
                    val intent = Intent(baseContext, StudentBusMapPage::class.java)
                    intent.putExtra("serviceId", student.myService)
                    intent.putExtra("studentId", student.userId)
                    intent.putExtra("studentName", student.name)
                    intent.putExtra("Lat", student.latitude)
                    intent.putExtra("Long", student.longitude)
                    intent.putExtra("schoolLatitude", student.schoolLatitude)
                    intent.putExtra("schoolLongitude", student.schoolLongitude)
                    startActivity(intent)
                }
                else{
                    Toast.makeText(baseContext, "There is no service where the student is registered.",Toast.LENGTH_SHORT).show()
                }
            }
        }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.children_list_layout)
        binding.recylerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        childrenAdapter.setOnSeeConnectorsClickListener(onClickSeeConnectors)
        childrenAdapter.setOnSeeConnectorsCardViewClickListener(onClickSeeConnectorsCardView)

        binding.recylerView.adapter = childrenAdapter
        getChildIds()
    }
    override fun getLayout() = R.layout.children_list_layout

    private fun getChildIds() {
        val childrenIdList = ArrayList<String>()
        rootRef.child(String.format("Users/%s/Children", uid)).addListenerForSingleValueEvent(object:
            ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot:DataSnapshot) {
                for (snapshot in dataSnapshot.children) {
                    val childId: String? = snapshot.getValue(String::class.java)
                    if (childId != null) {
                        childrenIdList.add(childId)
                    }
                }
                getChildrenObjects(childrenIdList)
                fun onCancelled(databaseError: DatabaseError) {
                    throw databaseError.toException()
                }
            }})
    }
    fun getChildrenObjects(childrenIds : ArrayList<String>){
       for(id in childrenIds){
           val child = StudentModel()
           rootRef.child(String.format("Users/%s", id)).addListenerForSingleValueEvent(object:
                ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }
                override fun onDataChange(dataSnapshot:DataSnapshot) {
                    child.age = dataSnapshot.child("age").value.toString()
                    child.email = dataSnapshot.child("email").value.toString()
                    child.imageUrl = dataSnapshot.child("imageUrl").value.toString()
                    child.latitude = dataSnapshot.child("latitude").value.toString()
                    child.longitude = dataSnapshot.child("longitude").value.toString()
                    child.name = dataSnapshot.child("name").value.toString()
                    child.password = dataSnapshot.child("password").value.toString()
                    child.schoolName = dataSnapshot.child("schoolName").value.toString()
                    child.status = dataSnapshot.child("status").value.toString()
                    child.telephoneNumber = dataSnapshot.child("telephoneNumber").value.toString()
                    child.parentNumber = dataSnapshot.child("parentNumber").value.toString()
                    child.homeAddress = dataSnapshot.child("homeAddress").value.toString()
                    child.myService = dataSnapshot.child("myService").value.toString()
                    child.userId = dataSnapshot.child("userId").value.toString()
                    child.schoolLatitude = dataSnapshot.child("schoolLatitude").value.toString()
                    child.schoolLongitude = dataSnapshot.child("schoolLongitude").value.toString()


                    childrenList.add(child)
                    if(id == childrenIds[childrenIds.size -1]){
                        childrenAdapter.setNewInstance(childrenList)
                        childrenAdapter.notifyDataSetChanged()
                    }
                    fun onCancelled(databaseError: DatabaseError) {
                        throw databaseError.toException()
                    }
                }
            })
        }
    }
    fun createAlertDialog(student: StudentModel){
           val mDialogView = layoutInflater.inflate(R.layout.base_alert_dialog_layout,null)
        mDialogView.message.text = getString(R.string.delete_child_message)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
            .setCancelable(false)
        val mAlertDialog = mBuilder?.show()
        mDialogView.alert_okey.setOnClickListener {
            childrenAdapter.editList(student)
            getParentIds(student)
            mAlertDialog?.dismiss()
        }
        mDialogView.alert_cancel.setOnClickListener {
            mAlertDialog?.dismiss()
        }
        mAlertDialog?.window?.setBackgroundDrawableResource(R.drawable.round_corner)
    }
    private fun getParentIds(student: StudentModel) {
        val parentIdList = ArrayList<String>()
        System.out.println("hmmm "+student.userId)
        rootRef.child(String.format("Users/%s/Parents", student.userId)).addListenerForSingleValueEvent(object:
            ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot:DataSnapshot) {
                for (snapshot in dataSnapshot.children) {
                    val parentId: String? = snapshot.getValue(String::class.java)
                    if (parentId != null) {
                        parentIdList.add(parentId)
                    }
                }
                System.out.println("buradadaaa"+parentIdList)
                getParentObjects(parentIdList,student)
                fun onCancelled(databaseError: DatabaseError) {
                    throw databaseError.toException()
                }
            }})
    }
    private fun getParentObjects(parentIdList:ArrayList<String>,student: StudentModel){
        for(id in parentIdList){
            rootRef.child(String.format("Users/%s", id)).addListenerForSingleValueEvent(object:
                ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val parentId = dataSnapshot.child("userId").value.toString()
                    if(parentId == uid){
                        System.out.println("****"+parentId)
                        parentIdList.remove(id)
                        editParentListOfChild(student.userId,parentIdList)
                        return
                    }
                }
            })
        }
    }
    private fun editParentListOfChild(childId:String,parentIdList: ArrayList<String>){
        rootRef.child("Users").child(childId).child("Parents").removeValue()
        reduceParentNum(true,childId,parentIdList.size)
        for(value in parentIdList){
            rootRef.child("Users").child(childId).child("Parents").child((parentIdList.indexOf(value)).toString()).setValue(value)
        }
        reduceParentNum(false,childId,parentIdList.size)
    }
    private fun reduceParentNum(zeroOrNot : Boolean, childId:String, size:Int){
        val uidRef = childId.let { rootRef.child("Users").child(it) }
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val childNumberStr = dataSnapshot.child("parentNumber").value.toString()
                var childNumber = childNumberStr.toInt()
                if(zeroOrNot)
                    childNumber = 0
                else
                    childNumber = size
                uidRef.child("parentNumber").setValue(childNumber.toString())
            }}
        uidRef.addListenerForSingleValueEvent(eventListener)
    }
}