package com.example.school_bus_tracker_app.ui.act_driver
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.school_bus_tracker_app.R
import com.example.school_bus_tracker_app.databinding.RecylerviewLayoutBinding
import com.example.school_bus_tracker_app.model.StudentModel
import com.example.school_bus_tracker_app.notification.NotificationPage
import com.example.school_bus_tracker_app.ui.act_driver.adapter.WaitingParticipantAdapter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.base_alert_dialog_layout.view.*

class WaitingParticipantListPage(a: String) : Fragment(){
    private var uid : String? = null
    private val serviceId : String = a
    val waitingParticipantList : MutableList<StudentModel> = mutableListOf()
    private val waitingParticipantAdapter = WaitingParticipantAdapter(mutableListOf(),serviceId)
    private val rootRef = FirebaseDatabase.getInstance().reference
    private var onClickSeeConnectors: WaitingParticipantAdapter.ItemClickListener? =
        object : WaitingParticipantAdapter.ItemClickListener {
            override fun onClickSeeConnectors(student: StudentModel) {
                acceptStudent(student)
            }
        }
    private var onClickSeeConnectorsDelete: WaitingParticipantAdapter.ItemClickDeleteListener? =
        object : WaitingParticipantAdapter.ItemClickDeleteListener {
            override fun onClickSeeConnectors(student: StudentModel) {
                createAlertDialog(student)
            }
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: RecylerviewLayoutBinding =
            DataBindingUtil.inflate(inflater, R.layout.recylerview_layout, container, false)
        binding.recylerView.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        waitingParticipantAdapter.setOnSeeConnectorsClickListener(onClickSeeConnectors)
        waitingParticipantAdapter.setOnSeeConnectorsDeleteClickListener(onClickSeeConnectorsDelete)
        binding.recylerView.adapter = waitingParticipantAdapter
        getWaitingStudentIds(serviceId)
        return binding.root

    }
    fun acceptStudent(student : StudentModel){
        var uidRef = rootRef.child("Users").child(student.userId)
        uidRef.child("myService").setValue(serviceId)

        val uidRef2 = rootRef.child("Services").child(serviceId)
        var serviceName = ""
        val eventListener3: ValueEventListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                serviceName = dataSnapshot.child("serviceName").value.toString()
                val lat = dataSnapshot.child("schoolLatitude").value.toString()
                val long = dataSnapshot.child("schoolLongitude").value.toString()
                rootRef.child("Users").child(student.userId).child("schoolLatitude").setValue(lat)
                rootRef.child("Users").child(student.userId).child("schoolLongitude").setValue(long)
                //öğrenciye bildirim gönderme
                uid = FirebaseAuth.getInstance().currentUser?.uid
                val a = NotificationPage()
                uid?.let { it1 -> a.createNotification(it1,student.userId,"Your registration request for the service named $serviceName \nhas been accepted.",serviceId)}

            }
        }
        uidRef2.addListenerForSingleValueEvent(eventListener3)

        uidRef = rootRef.child("Services").child(serviceId)
        val eventListener2: ValueEventListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val participantNumberStr = dataSnapshot.child("participantNumber").value.toString()
                    var participantNumber = participantNumberStr.toInt()
                    uidRef.child("participants").child(participantNumber.toString()).setValue(student.userId)
                    participantNumber += 1
                    val uidRef2 = serviceId.let { rootRef.child("Services").child(it) }
                    uidRef2.child("participantNumber").setValue(participantNumber.toString())
                }
            }
        uidRef.addListenerForSingleValueEvent(eventListener2)

    }
    private fun getWaitingStudentIds(serviceId : String) {
        val studentIdList = ArrayList<String>()
        rootRef.child(String.format("Services/%s/waitingParticipants", serviceId)).addListenerForSingleValueEvent(object:
            ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (snapshot in dataSnapshot.children) {
                    val studentId: String? = snapshot.getValue(String::class.java)
                    if (studentId != null) {
                        studentIdList.add(studentId)
                    }
                }
                getStudentObjects(studentIdList)
            }})
    }
    fun getStudentObjects(studentIds : ArrayList<String>){
        for(id in studentIds){
            val student = StudentModel()
            Log.d("-------","$id ${studentIds.size}")
            rootRef.child(String.format("Users/%s", id)).addListenerForSingleValueEvent(object:
                ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    student.userId = dataSnapshot.child("userId").value.toString()
                    student.name = dataSnapshot.child("name").value.toString()
                    student.schoolName = dataSnapshot.child("schoolName").value.toString()
                    student.age = dataSnapshot.child("age").value.toString()
                    student.telephoneNumber =
                        dataSnapshot.child("telephoneNumber").value.toString()
                    Log.d("Eklendi", "${student.name} ${waitingParticipantList.size}")
                    waitingParticipantList.add(student)
                    waitingParticipantAdapter.notifyDataSetChanged()
                    for (i in waitingParticipantList) {
                        Log.d("çocuk", " ${i.name} ${waitingParticipantList.size}")
                    }
                    waitingParticipantAdapter.setNewInstance(waitingParticipantList)
                    waitingParticipantAdapter.notifyDataSetChanged()
                }
            })
        }
    }
    fun createAlertDialog(student: StudentModel){
        val mDialogView = layoutInflater.inflate(R.layout.base_alert_dialog_layout,null)
        mDialogView.message.text = getString(R.string.rejectRequestMessage)
        val mBuilder = context?.let {
            AlertDialog.Builder(it)
                .setView(mDialogView)
                .setCancelable(false)
        }
        val mAlertDialog = mBuilder?.show()
        mDialogView.alert_okey.setOnClickListener {
            waitingParticipantAdapter.editList(student)
            mAlertDialog?.dismiss()
        }
        mDialogView.alert_cancel.setOnClickListener {
            mAlertDialog?.dismiss()
        }
        mAlertDialog?.window?.setBackgroundDrawableResource(R.drawable.round_corner)
    }

}