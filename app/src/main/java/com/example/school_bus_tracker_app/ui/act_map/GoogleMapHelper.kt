package com.example.school_bus_tracker_app.ui.act_map

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.example.school_bus_tracker_app.R
import com.google.android.gms.maps.CameraUpdate
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class GoogleMapHelper {
    companion object {
        private const val ZOOM_LEVEL = 18
        private const val TILT_LEVEL = 25
    }
    /**
     * @param latLng in which position to Zoom the camera.
     * @return the [CameraUpdate] with Zoom and Tilt level added with the given position.
     */
    fun buildCameraUpdate(latLng: LatLng): CameraUpdate {
        val cameraPosition = CameraPosition.Builder()
            .target(latLng)
            .tilt(TILT_LEVEL.toFloat())
            .zoom(ZOOM_LEVEL.toFloat())
            .build()
        return CameraUpdateFactory.newLatLngZoom(latLng,12f)//newCameraPosition(cameraPosition)
    }
    /**
     * @param position where to draw the [com.google.android.gms.maps.model.Marker]
     * @return the [MarkerOptions] with given properties added to it.
     */
    fun getDriverMarkerOptions(resource:Int,position: LatLng): MarkerOptions {
        val options = getMarkerOptions(resource, position)
        options.flat(true)
        return options
    }
    private fun getMarkerOptions(resource: Int, position: LatLng): MarkerOptions {
        return MarkerOptions()
            .icon(BitmapDescriptorFactory.fromResource(resource))
            .position(position)
    }
}