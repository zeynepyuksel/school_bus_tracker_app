package com.example.school_bus_tracker_app.ui.act_profile_page

import android.os.Bundle
import com.example.school_bus_tracker_app.R
import com.example.school_bus_tracker_app.core.BaseActivity
import com.example.school_bus_tracker_app.databinding.ProfilePageLayoutBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class ProfilePage : BaseActivity<ProfilePageLayoutBinding>() {
    private val rootRef = FirebaseDatabase.getInstance().reference
    private var uid = FirebaseAuth.getInstance().currentUser?.uid

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.profile_page_layout)
        showProfieInfo()

        binding.editPageBtn.setOnClickListener{
            updateUI2<EditProfilePage>()
        }
        binding.backPageBtn.setOnClickListener {
            onBackPressed()
        }

    }
    override fun getLayout() = R.layout.profile_page_layout
    private fun showProfieInfo(){
        var uidRef = uid?.let { rootRef.child("Users").child(it) }
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                }
                else{
                    val userType = dataSnapshot.child("userType").value.toString()
                    val name = dataSnapshot.child("name").value.toString()
                    val age = dataSnapshot.child("age").value.toString()
                    val telephoneNumber = dataSnapshot.child("telephoneNumber").value.toString()
                    binding.nameSurname.text = name
                    binding.age.text = age
                    binding.telephoneNumber.text = telephoneNumber
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {}
        }
        uidRef?.addListenerForSingleValueEvent(eventListener)

    }
}