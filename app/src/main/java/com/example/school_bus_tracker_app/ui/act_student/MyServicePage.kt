@file:Suppress("DEPRECATION")

package com.example.school_bus_tracker_app.ui.act_student

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import com.example.school_bus_tracker_app.R
import com.example.school_bus_tracker_app.core.BaseActivity
import com.example.school_bus_tracker_app.databinding.StudentServicePageLayoutBinding
import com.example.school_bus_tracker_app.ui.act_map.GoogleMapHelper
import com.example.school_bus_tracker_app.ui.act_map.LatLngInterpolator
import com.example.school_bus_tracker_app.ui.act_map.MarkerAnimationHelper
import com.example.school_bus_tracker_app.ui.act_map.UiHelper
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.base_alert_dialog_layout.view.*

@Suppress("DEPRECATION")
class MyServicePage : BaseActivity<StudentServicePageLayoutBinding>(), OnMapReadyCallback,GoogleMap.OnMarkerClickListener{
    private lateinit var locationCallback: LocationCallback
    private var currentPositionMarker: Marker? = null
    private val googleMapHelper = GoogleMapHelper()
    private val markerAnimationHelper = MarkerAnimationHelper()
    private lateinit var locationRequest: LocationRequest
    private val uiHelper = UiHelper()
    private var locationFlag = true
    private var homeLocationFlag = true
    private var schoolLocationFlag = true
    lateinit var cameraHome : ImageButton
    lateinit var cameraBus : ImageButton
    lateinit var cameraSchool : ImageButton
    private lateinit var googleMap: GoogleMap
    private lateinit var locationProviderClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    private lateinit var latLng : LatLng
    private lateinit var schoolLatLng : LatLng
    private lateinit var busLatLng : LatLng
    private lateinit var markerOptions : MarkerOptions
    private var uid : String = ""
    private var serviceId : String = ""
    lateinit var drive : String
    lateinit var driverName : TextView
    lateinit var serviceName: TextView
    lateinit var deleteServiceBtn: Button
    lateinit var callDriver: Button
    lateinit var zoom : ZoomControls
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.student_service_page_layout)
        uid = FirebaseAuth.getInstance().currentUser!!.uid
        driverName = findViewById(R.id.driver_name)
        serviceName = findViewById(R.id.student_service_txt)
        deleteServiceBtn = findViewById(R.id.delete_service_btn)
        cameraHome = findViewById(R.id.studentCameraHome)
        cameraBus = findViewById(R.id.studentCameraBus)
        cameraSchool = findViewById(R.id.studentCameraSchool)
        callDriver = findViewById(R.id.driverCall)
        zoom = findViewById(R.id.zoom)
        deleteServiceBtn.setOnClickListener {
            createAlertDialog()
        }
        takeServiceId()
        callDriver.setOnClickListener {
            callDriverr()
        }
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.student_map) as SupportMapFragment
        mapFragment.getMapAsync { googleMap = it }
        locationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        zoom.setOnZoomOutClickListener { googleMap.animateCamera(CameraUpdateFactory.zoomOut()) }
        zoom.setOnZoomInClickListener { googleMap.animateCamera(CameraUpdateFactory.zoomIn()) }

        val rootRef = FirebaseDatabase.getInstance().reference
        val uidRef = uid.let { rootRef.child("Users").child(it) }
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                    Toast.makeText(
                        this@MyServicePage,
                        getString(R.string.student_not_found),
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    val latitude = dataSnapshot.child("latitude").value.toString()
                    val longitude = dataSnapshot.child("longitude").value.toString()
                    latLng = LatLng(latitude.toDouble(), longitude.toDouble())

                    val schoolLatitude = dataSnapshot.child("schoolLatitude").value.toString()
                    val schoolLongitude = dataSnapshot.child("schoolLongitude").value.toString()
                    schoolLatLng = LatLng(schoolLatitude.toDouble(), schoolLongitude.toDouble())

                    createLocationCallback()
                    locationRequest = uiHelper.getLocationRequest()
                    if (!uiHelper.isPlayServicesAvailable(this@MyServicePage)) {
                        Toast.makeText(this@MyServicePage, "Play Services did not installed!", Toast.LENGTH_SHORT).show()
                        finish()
                    } else requestLocationUpdate()
                }
            }
        }
        uidRef.addListenerForSingleValueEvent(eventListener)
        cameraHome.setOnClickListener {animateCamera(latLng)}
        cameraBus.setOnClickListener {
            Log.d("*/*/*/*/",drive)
            if(drive == "on")
                animateCamera(busLatLng)
        }
        cameraSchool.setOnClickListener {animateCamera(schoolLatLng)}
    }

    private fun callDriverr(){
        val rootRef = FirebaseDatabase.getInstance().reference
        val uidRef = serviceId.let { rootRef.child("Services").child(it) }
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                    Toast.makeText(this@MyServicePage,getString(R.string.service_not_found), Toast.LENGTH_LONG).show()
                } else {
                    val driverId = dataSnapshot.child("driverId").value.toString()
                    takeDriverNumber(driverId)
                }
            }
        }
        uidRef.addListenerForSingleValueEvent(eventListener)
    }
    private fun takeDriverNumber(driverID:String){
        val rootRef = FirebaseDatabase.getInstance().reference
        val uidRef = driverID.let { rootRef.child("Users").child(it) }
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                    Toast.makeText(this@MyServicePage,getString(R.string.not_found_driver), Toast.LENGTH_LONG).show()
                } else {
                    val telNumber = dataSnapshot.child("telephoneNumber").value.toString()
                    val intent =
                        Intent(Intent.ACTION_DIAL, Uri.parse("tel:$telNumber"))
                    startActivity(intent)
                }
            }
        }
        uidRef.addListenerForSingleValueEvent(eventListener)
    }
    private fun editDriverInfo(){
        val rootRef = FirebaseDatabase.getInstance().reference
        val uidRef = serviceId.let { rootRef.child("Services").child(it) }
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                    Toast.makeText(this@MyServicePage,getString(R.string.not_found_driver),Toast.LENGTH_LONG).show()
                }
                else{
                    val driverId = dataSnapshot.child("driverId").value.toString()
                    val uidRef2 =  rootRef.child("Users").child(driverId)
                    val eventListener2 : ValueEventListener = object : ValueEventListener{
                        @SuppressLint("SetTextI18n")
                        override fun onDataChange(snapshot: DataSnapshot) {
                            val name =  snapshot.child("name").value.toString()
                            val age = snapshot.child("age").value.toString()
                            driverName.text = "$name($age)"
                        }
                        override fun onCancelled(error: DatabaseError) {
                        }
                    }
                    uidRef2.addListenerForSingleValueEvent(eventListener2)
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {}
        }
        uidRef.addListenerForSingleValueEvent(eventListener)
    }
    private fun deleteService(){
        val rootRef = FirebaseDatabase.getInstance().reference
        val uidRef = uid.let { rootRef.child("Users").child(it) }
        uidRef.child("myService").setValue(" ")
        val list = ArrayList<String>()
        var uidRef2 = serviceId.let { rootRef.child("Services").child(it) }.child("participants")
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (snapshot in dataSnapshot.children) {
                    val studentId = snapshot.getValue(String::class.java)
                    if(studentId != uid){
                        if (studentId != null) {
                            list.add(studentId)
                        }
                    }
                    uidRef2.removeValue()
                    for(i in list){
                        rootRef.child("Services").child(serviceId).child("participants").child((list.indexOf(i)).toString()).setValue(i)
                    }
                    uidRef2 = serviceId.let { rootRef.child("Services").child(it) }.child("participantNumber")
                    uidRef2.setValue((list.size).toString())
                }
            }
        }
        uidRef2.addListenerForSingleValueEvent(eventListener)


    }
    private fun takeServiceId(){
        val rootRef = FirebaseDatabase.getInstance().reference
        val uidRef = uid.let { rootRef.child("Users").child(it) }
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                    Toast.makeText(this@MyServicePage,getString(R.string.student_not_found), Toast.LENGTH_LONG).show()
                } else {
                    val serviceId = dataSnapshot.child("myService").value.toString()
                    this@MyServicePage.serviceId = serviceId
                    serviceInfo()
                    editDriverInfo()
                }
            }
        }
        uidRef.addListenerForSingleValueEvent(eventListener)
    }
    private fun serviceInfo(){
        val rootRef = FirebaseDatabase.getInstance().reference
        val uidRef = serviceId.let { rootRef.child("Services").child(it) }
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                    Toast.makeText(this@MyServicePage,getString(R.string.service_not_found), Toast.LENGTH_LONG).show()
                } else {
                    val serviceNameTxt = dataSnapshot.child("serviceName").value.toString()
                    serviceName.text = serviceNameTxt
                }
            }
        }
        uidRef.addListenerForSingleValueEvent(eventListener)
    }

    override fun getLayout() = R.layout.student_service_page_layout

    private fun createAlertDialog(){
        val mDialogView = layoutInflater.inflate(R.layout.base_alert_dialog_layout,null)
        mDialogView.message.text = getString(R.string.quitServiceMessage)
        val mBuilder = AlertDialog.Builder(this)
                .setView(mDialogView)
                .setCancelable(false)
        val mAlertDialog = mBuilder?.show()
        mDialogView.alert_okey.setOnClickListener {
            deleteService()
            updateUI<StudentHomePage>()
            mAlertDialog?.dismiss()
        }
        mDialogView.alert_cancel.setOnClickListener {
            mAlertDialog?.dismiss()
        }
        mAlertDialog?.window?.setBackgroundDrawableResource(R.drawable.round_corner)
    }
    override fun onMapReady(p0: GoogleMap) {
        googleMap = p0
        googleMap.mapType = GoogleMap.MAP_TYPE_TERRAIN
        googleMap.uiSettings.isZoomControlsEnabled = true
        googleMap.setOnMarkerClickListener(this)
    }

    override fun onMarkerClick(p0: Marker?) = false
    private fun createLocationCallback() {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)
                if (locationResult!!.lastLocation == null) return
                val u =
                    LatLng(locationResult.lastLocation.latitude, locationResult.lastLocation.longitude)
                val rootRef = FirebaseDatabase.getInstance().reference
                val uidRef = serviceId.let { rootRef.child("Services").child(it) }
                val eventListener: ValueEventListener = object : ValueEventListener {
                    override fun onCancelled(error: DatabaseError) {
                        TODO("Not yet implemented")
                    }
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        if (!dataSnapshot.exists()) {
                            Toast.makeText(
                                this@MyServicePage,
                                getString(R.string.service_not_found),
                                Toast.LENGTH_LONG
                            ).show()
                        } else {
                            drive = dataSnapshot.child("drive").value.toString()
                            val latitude = dataSnapshot.child("latitude").value.toString()
                            val longitude = dataSnapshot.child("longitude").value.toString()
                            if(homeLocationFlag){
                                animateCamera(latLng)
                                showOrAnimateMarker(latLng,0)
                                homeLocationFlag = false
                            }
                            if(schoolLocationFlag){
                                showOrAnimateMarker(schoolLatLng,3)
                                schoolLocationFlag = false
                            }
                            if (drive == "on") {
                                showOrAnimateMarker(latLng,0)
                                busLatLng = LatLng(latitude.toDouble(), longitude.toDouble())
                                Log.e("Location", busLatLng.latitude.toString() + " , " + busLatLng.longitude)
                                if (locationFlag && drive=="on") {
                                    locationFlag = false
                                    animateCamera(busLatLng)
                                }
                                showOrAnimateMarker(busLatLng,1)
                            }
                            else{
                                if(currentPositionMarker != null){
                                    currentPositionMarker!!.isVisible = false
                                    currentPositionMarker = null
                                    locationFlag = true
                                    animateCamera(latLng)
                                }
                                showOrAnimateMarker(latLng,0)
                            }
                        }
                    }
                }
                uidRef.addListenerForSingleValueEvent(eventListener)
            }}
    }
    private fun showOrAnimateMarker(latLng: LatLng,count:Int) {
        if (currentPositionMarker == null && count == 1)
            currentPositionMarker = googleMap.addMarker(googleMapHelper.getDriverMarkerOptions(R.drawable.bus,latLng))
        else if(count == 0){
            googleMap.addMarker(googleMapHelper.getDriverMarkerOptions(R.drawable.house,latLng))
        }
        else if(count == 3){
            googleMap.addMarker(googleMapHelper.getDriverMarkerOptions(R.drawable.school2,schoolLatLng))
        }
        else markerAnimationHelper.animateMarkerToGB(currentPositionMarker!!, latLng, LatLngInterpolator.Spherical())
    }
    private fun animateCamera(latLng: LatLng) {
        val cameraUpdate = googleMapHelper.buildCameraUpdate(latLng)
        //googleMap.animateCamera(cameraUpdate, 10, null)
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12f))
    }
    @SuppressLint("MissingPermission")
    private fun requestLocationUpdate() {
        if (!uiHelper.isHaveLocationPermission(this)) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)
            return
        }
        locationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper())
    }
    companion object {
        private const val MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 2200
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION) {
            val value = grantResults[0]
            if (value == PackageManager.PERMISSION_DENIED) {
                Toast.makeText(this, "Location Permission denied", Toast.LENGTH_SHORT).show()
                finish()
            } else if (value == PackageManager.PERMISSION_GRANTED) requestLocationUpdate()
        }
    }

}