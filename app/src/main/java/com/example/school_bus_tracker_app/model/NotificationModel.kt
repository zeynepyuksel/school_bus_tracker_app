package com.example.school_bus_tracker_app.model

class NotificationModel {
    var from: String = ""
    var to: String = ""
    var fromName: String = ""
    var serviceId : String = ""
    var text: String = ""
    var notificationId: String = ""
}