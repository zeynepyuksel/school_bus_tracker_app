package com.example.school_bus_tracker_app.ui.act_parent.profilePage

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.Toast
import com.example.school_bus_tracker_app.R
import com.example.school_bus_tracker_app.core.BaseActivity
import com.example.school_bus_tracker_app.databinding.ProfileEditPageLayoutBinding

class ParentEditProfilePage : BaseActivity<ProfileEditPageLayoutBinding>() {

    @SuppressLint("ShowToast")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.profile_edit_page_layout)

        binding.backPageBtn.setOnClickListener{
            onBackPressed()
        }
        binding.saveBtn.setOnClickListener {
            Toast.makeText(baseContext, "Saved", Toast.LENGTH_LONG)
            updateUI<ParentProfilePage>()
        }

    }
    override fun getLayout() = R.layout.profile_edit_page_layout
}
