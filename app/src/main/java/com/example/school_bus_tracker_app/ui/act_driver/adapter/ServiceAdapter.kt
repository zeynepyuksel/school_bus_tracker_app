package com.example.school_bus_tracker_app.ui.act_driver.adapter
import android.widget.Button
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.example.school_bus_tracker_app.R
import com.example.school_bus_tracker_app.model.ServiceModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class ServiceAdapter(data: MutableList<ServiceModel>) : BaseQuickAdapter<ServiceModel, BaseViewHolder>(R.layout.service_card_view_layout) {
    private val uid = FirebaseAuth.getInstance().currentUser!!.uid
    private val rootRef = FirebaseDatabase.getInstance().reference
    private var seeConnectorsClickListener: ItemClickListener? = null
    private var seeConnectorsClickDeleteListener: ItemClickDeleteListener? = null

    override fun convert(holder: BaseViewHolder, item: ServiceModel) {
        holder.setText(R.id.txtServiceName,item.serviceName)
            .setText(R.id.participants_number,item.participantNumber)
            .setText(R.id.participants_request_number,item.participantRequestNumber)

        holder.getView<Button>(R.id.service_delete_btn).setOnClickListener {
            seeConnectorsClickDeleteListener?.onClickSeeConnectors(item)
        }
        holder.getView<Button>(R.id.service_edit_btn).setOnClickListener {
            seeConnectorsClickListener?.onClickSeeConnectors(item)
        }
    }
    public fun deleteService(item:ServiceModel){
        val position = data.indexOf(item)
        data.removeAt(position)
        notifyDataSetChanged()
        rootRef.child("Users").child(uid).child("Services").removeValue()
        reduceServiceNum(true)
        for(value in data){
            rootRef.child("Users").child(uid).child("Services").child((data.indexOf(value)).toString()).setValue(value.serviceId)
        }
        reduceServiceNum(false)
        rootRef.child("Services").child(item.serviceId).removeValue()
    }
    private fun reduceServiceNum(zeroOrNot : Boolean){
        val uidRef = uid.let { rootRef.child("Users").child(it) }
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val serviceNumberStr = dataSnapshot.child("serviceNumber").value.toString()
                var serviceNumber = serviceNumberStr.toInt()
                if(zeroOrNot)
                    serviceNumber = 0
                else
                    serviceNumber = data.size
                uidRef.child("serviceNumber").setValue(serviceNumber.toString())
            }}
        uidRef.addListenerForSingleValueEvent(eventListener)
    }
    fun setOnSeeConnectorsClickListener(listener: ItemClickListener?) {
        seeConnectorsClickListener = listener
    }

    interface ItemClickListener {
        fun onClickSeeConnectors(service : ServiceModel)
    }
    fun setOnSeeConnectorsClickDeleteListener(listener: ItemClickDeleteListener?) {
        seeConnectorsClickDeleteListener = listener
    }

    interface ItemClickDeleteListener {
        fun onClickSeeConnectors(service : ServiceModel)
    }
}