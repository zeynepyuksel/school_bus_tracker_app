package com.example.school_bus_tracker_app.model

class ServiceModel{
    var serviceId:String=""
    var serviceName:String=""
    var schoolName:String=""
    var driverId:String=""
    var participantNumber:String="0"
    var participantRequestNumber:String="0"
    var latitude:String =""
    var longitude:String=""
    var schoolAddress:String=""
    var where:String=""
}
