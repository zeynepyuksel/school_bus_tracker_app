package com.example.school_bus_tracker_app.ui.act_driver

import android.app.AlertDialog
import android.os.Bundle
import android.widget.Toast
import com.example.school_bus_tracker_app.R
import com.example.school_bus_tracker_app.core.BaseActivity
import com.example.school_bus_tracker_app.databinding.DriverHomePageLayoutBinding
import com.example.school_bus_tracker_app.notification.MyFirebaseMessagingService
import com.example.school_bus_tracker_app.notification.NotificationPage
import com.example.school_bus_tracker_app.ui.act_profile_page.ProfilePage
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.create_service_dialog_layout.view.*


class DriverHomePage: BaseActivity<DriverHomePageLayoutBinding>() {
    private var mDatabaseReference: DatabaseReference? = null
    private var mDatabase: FirebaseDatabase? = null
    private var uid : String = " "

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.driver_home_page_layout)
        uid = FirebaseAuth.getInstance().currentUser!!.uid
        controlNotification(uid)

        binding.createService.setOnClickListener {
            createServiceDialog()
        }
        binding.myServices.setOnClickListener {
            lookServiceNum()
        }
        binding.userProfile.setOnClickListener {
            updateUI2<ProfilePage>()
        }
        binding.driverExit.setOnClickListener {
            finish()
            exit()
        }
        binding.notification.setOnClickListener{
            updateUI2<NotificationPage>()
        }
    }
    private fun controlNotification(userId:String){
        val rootRef = FirebaseDatabase.getInstance().reference
        println("uid : $userId")
        val uidRef = userId.let { rootRef.child("Users").child(it) } .child("Notifications")
        uidRef.keepSynced(true)
        uidRef.addChildEventListener(object : ChildEventListener {
            override fun onCancelled(error: DatabaseError) {
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
            }

            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                val str:String = snapshot.value.toString()
                println("str : $str uid $userId")
                val gone = "false"
                val c = FirebaseDatabase.getInstance().reference
                val uidReference2 = c.child("Notifications").child(str)
                val eventListener2: ValueEventListener = object : ValueEventListener {
                    override fun onCancelled(error: DatabaseError) {
                        TODO("Not yet implemented")
                    }
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        if (!dataSnapshot.exists()) {
                        } else {
                            val gone2 = dataSnapshot.child("gone").value.toString()
                            val from = dataSnapshot.child("fromName").value.toString()
                            val message = dataSnapshot.child("text").value.toString()
                            if(gone2 == gone){
                                val controlNotification = MyFirebaseMessagingService()
                                controlNotification.showNotification(applicationContext,from,message)
                                uidReference2.child("gone").setValue("true")
                            }
                        }
                    }
                }
                uidReference2.addListenerForSingleValueEvent(eventListener2)
            }
            override fun onChildRemoved(snapshot: DataSnapshot) {
            }
        })
    }

    override fun getLayout() = R.layout.driver_home_page_layout

    private fun createServiceDialog(){
        val mDialogView = layoutInflater.inflate(R.layout.create_service_dialog_layout,null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
            .setCancelable(false)
        val mAlertDialog = mBuilder.show()
        mDialogView.okey.setOnClickListener {
            val serviceName = mDialogView.service_name.editText?.text.toString().trim()
            val schoolName = mDialogView.service_school_name.editText?.text.toString().trim()
            val schoolAddress = mDialogView.schoolAddress.editText?.text.toString().trim()

            if (serviceName.isEmpty() or schoolName.isEmpty() or schoolAddress.isEmpty()) {
                if(serviceName.isEmpty())
                    mDialogView.service_name.error = getString(R.string.service_name_cant_empty)
                if(schoolName.isEmpty())
                    mDialogView.service_school_name.error = getString(R.string.school_name_cant_empty)
                if(schoolAddress.isEmpty())
                    mDialogView.schoolAddress.error = getString(R.string.school_address_cant_empty)
            }
            else {
                mDialogView.service_name.error = null
                mDialogView.service_school_name.error = null
                mDialogView.schoolAddress.error = null
                createSchoolService(serviceName,schoolName,schoolAddress)
                mAlertDialog.dismiss()
            }
    }
        mDialogView.cancel.setOnClickListener {
            mAlertDialog.dismiss()
        }
        mAlertDialog.window?.setBackgroundDrawableResource(R.drawable.round_corner)
    }
    private fun createSchoolService(serviceName:String, schoolName:String, schoolAddress:String){
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference.child("Services")
        val serviceDb = mDatabaseReference!!.push()
        serviceDb.child("serviceName").setValue(serviceName)
        serviceDb.child("schoolName").setValue(schoolName)
        serviceDb.child("serviceId").setValue(serviceDb.key)
        serviceDb.child("participantNumber").setValue("0")
        serviceDb.child("participantRequestNumber").setValue("0")
        serviceDb.child("driverId").setValue(uid)
        serviceDb.child("drive").setValue("off")
        serviceDb.child("schoolAddress").setValue(schoolAddress)
        serviceDb.child("where").setValue("goSchool")
        serviceDb.key?.let { getLatLog(schoolAddress, it,"Services","schoolLatitude","schoolLongitude") }

        val rootRef = FirebaseDatabase.getInstance().reference
        var uidRef = uid.let { rootRef.child("Users").child(it) }
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                    Toast.makeText(this@DriverHomePage,getString(R.string.not_found_driver),Toast.LENGTH_LONG).show()
                }
                else{
                    val serviceNumberStr = dataSnapshot.child("serviceNumber").value.toString()
                    var serviceNumber = serviceNumberStr.toInt()
                    uidRef = uidRef.child("Services")
                    uidRef.child(serviceNumber.toString()).setValue(serviceDb.key)
                    serviceNumber += 1
                    uidRef = uid.let { rootRef.child("Users").child(it) }
                    uidRef.child("serviceNumber").setValue(serviceNumber.toString())
                    }
                    Toast.makeText(this@DriverHomePage,getString(R.string.service_added),Toast.LENGTH_LONG).show()
            }
            override fun onCancelled(databaseError: DatabaseError) {}
        }
        uidRef.addListenerForSingleValueEvent(eventListener)

    }
    private fun lookServiceNum(){
        val rootRef = FirebaseDatabase.getInstance().reference
        val uidRef = uid.let { rootRef.child("Users").child(it) }
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                    Toast.makeText(this@DriverHomePage,getString(R.string.not_found_driver),Toast.LENGTH_LONG).show()
                }
                else{
                    val serviceNumberStr = dataSnapshot.child("serviceNumber").value.toString()
                    val serviceNumber = serviceNumberStr.toInt()
                    if(serviceNumber == 0){
                        Toast.makeText(this@DriverHomePage,getString(R.string.dont_have_service),Toast.LENGTH_LONG).show()
                    }
                    else{
                        updateUI2<DriverServiceListPage>()
                        return
                    }
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {}
        }
        uidRef.addListenerForSingleValueEvent(eventListener)
    }
}