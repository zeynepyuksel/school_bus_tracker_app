package com.example.school_bus_tracker_app.core

import android.app.Activity
import android.app.Notification
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.Address
import android.location.Geocoder
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.example.school_bus_tracker_app.R
import com.example.school_bus_tracker_app.databinding.ViewCustomDialogBinding
import com.example.school_bus_tracker_app.ui.act_splash.SplashPage
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.add_child_layout.view.*
import kotlinx.android.synthetic.main.create_service_dialog_layout.view.*
import java.io.IOException

abstract class BaseActivity<DB : ViewDataBinding>() : DialogProviderActivity() {
    @LayoutRes
    abstract fun getLayout(): Int


    val binding by lazy {
        DataBindingUtil.setContentView(this, getLayout()) as DB
    }
    inline fun <reified T : Activity> updateUI() {
        val intent = Intent(this, T::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
        finish()
    }
    inline fun <reified T : Activity> updateUI2() {
        val intent = Intent(this, T::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }
    fun exit(){
        val sharedPreferences = this.getSharedPreferences(packageName,Context.MODE_PRIVATE)
        sharedPreferences.edit().remove("userId").apply()
        finishAffinity()
        val intent = Intent(this, SplashPage::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
        finish()
    }
    fun resizeBitmap(drawableName: String?, width: Int, height: Int): Bitmap? {
        val imageBitmap: Bitmap = BitmapFactory.decodeResource(
            resources,
            resources.getIdentifier(drawableName, "drawable", packageName)
        )
        return Bitmap.createScaledBitmap(imageBitmap, width, height, false)
    }

    fun getLatLog(address:String,userId:String, path:String, lat:String, long:String){
        val location= address
        if(location != ""){
            var adressList : List<Address>? = null
            val geocoder : Geocoder = Geocoder(this)
            try {
                adressList=geocoder.getFromLocationName(location,1);
            } catch (e : IOException) {
                e.printStackTrace()
            }
            val location= adressList?.get(0)
            System.out.println("lat "+ (location?.latitude)+"long "+ (location?.longitude))
            val rootRef = FirebaseDatabase.getInstance().reference
            val uidRef = userId.let { rootRef.child(path).child(it) }
            uidRef.child(lat).setValue(location?.latitude.toString())
            uidRef.child(long).setValue(location?.longitude.toString())
        }
    }
    fun showCustomDialog(
        imageRes: Int? = null,
        title: String,
        message: String? = null,
        showInput: Boolean = false,
        showCloseButton: Boolean = false,
        inputHint: String = "",
        inputText: String = "",
        inputHandler: ((input: String) -> Unit)? = null,
        leftButtonTextRes: Int? = null,
        leftButtonAction: Notification.Action? = null,
        rightButtonTextRes: Int,
        rightButtonAction: Notification.Action? = null
    ) {
        dismissDialog()

        //INFLATE CUSTOM VIEW
        val binding = ViewCustomDialogBinding.inflate(
            LayoutInflater.from(this), null, false
        )

        //CLOSE BUTTON
        if (showCloseButton) {
            binding.buttonClose.visibility = View.VISIBLE
            binding.buttonClose.setOnClickListener { dialog?.dismiss() }
        } else {
            binding.buttonClose.visibility = View.INVISIBLE
        }

        //ICON
        if (imageRes == null) {
            binding.imageviewIcon.visibility = View.GONE
        } else {
            binding.imageviewIcon.visibility = View.VISIBLE
            binding.imageviewIcon.setImageResource(imageRes)
        }

        //TITLE
        binding.textviewTitle.text = title

        //MESSAGE
        if (message == null) {
            binding.textviewMessage.visibility = View.GONE
        } else {
            binding.textviewMessage.visibility = View.VISIBLE
            binding.textviewMessage.text = message
        }

        //INPUT FIELD
        if (showInput) {
            binding.inputView.visibility = View.VISIBLE
            binding.inputView.hint = inputHint
            binding.inputView.text = inputText
            /*binding.inputView.addTextWatcher(
                object : TextWatcher {
                    override fun afterTextChanged(p0: Editable?) {}
                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                        binding.buttonRight.isEnabled =
                            binding.inputView.text.isNotBlank()
                    }

                }
            )*/
        } else {
            binding.inputView.visibility = View.GONE
        }

        //ACTION BUTTONS
        if (leftButtonTextRes == null) {
            binding.buttonLeft.visibility = View.GONE
        } else {
            binding.buttonLeft.visibility = View.VISIBLE

            binding.buttonLeft.text = getString(leftButtonTextRes)
            binding.buttonLeft.setOnClickListener {
                dialog?.dismiss()

                if (leftButtonAction == null) {
                    return@setOnClickListener
                }

                try {
                    leftButtonAction.run{}
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }

        binding.buttonRight.text = getString(rightButtonTextRes)
        binding.buttonRight.setOnClickListener {
            //INVOKE INPUT HANDLER IF ANY
            inputHandler?.invoke(binding.inputView.text.toString())

            dialog?.dismiss()

            if (rightButtonAction == null) {
                return@setOnClickListener
            }

            try {
                rightButtonAction.run {  }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        //CREATE & SHOW DIALOG
        dialog = AlertDialog.Builder(this)
            .setView(binding.root)
            .setCancelable(false)
            .create()
        dialog?.window?.setBackgroundDrawableResource(android.R.color.transparent)
        dialog?.show()
    }
    private fun dismissDialog() {
        if (dialog != null) {
            dialog?.dismiss()
            dialog = null
        }
    }
}