package com.example.school_bus_tracker_app.ui.act_driver

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.viewpager.widget.ViewPager
import com.example.school_bus_tracker_app.R
import com.example.school_bus_tracker_app.core.BaseActivity
import com.example.school_bus_tracker_app.databinding.ServiceDetailPageLayoutBinding
import com.example.school_bus_tracker_app.notification.NotificationPage
import com.example.school_bus_tracker_app.ui.act_driver.adapter.ViewPagerAdapter
import com.example.school_bus_tracker_app.ui.act_map.GoogleMapHelper
import com.example.school_bus_tracker_app.ui.act_map.MarkerAnimationHelper
import com.example.school_bus_tracker_app.ui.act_map.UiHelper
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.tabs.TabLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.base_alert_dialog_layout.view.*


@Suppress("DEPRECATION")
class ServiceDetailPage : BaseActivity<ServiceDetailPageLayoutBinding>(), OnMapReadyCallback,GoogleMap.OnMarkerClickListener {

    private var serviceId : String = " "
    private lateinit var mMap: GoogleMap
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    private lateinit var markerOptions : MarkerOptions
    private var markerList = HashMap<String,Marker>()
    private lateinit var locationCallback: LocationCallback
    private lateinit var locationRequest: LocationRequest
    private  lateinit var participantsPage : ParticipantListPage
    private var uid : String = " "
    private var locationFlag = true
    private val googleMapHelper = GoogleMapHelper()
    private val markerAnimationHelper = MarkerAnimationHelper()
    private val uiHelper = UiHelper()
    private var currentPositionMarker: Marker? = null
    private val mHandler: Handler = Handler()
    private lateinit var mapFragment : SupportMapFragment
    private var driverMarkerNum =0
    val rootRef = FirebaseDatabase.getInstance().reference

    override fun getLayout() = R.layout.service_detail_page_layout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.service_detail_page_layout)
        val shareButton : Button = findViewById(R.id.shareButton)
        val startService: Button = findViewById(R.id.start_service)
        val finishService: Button = findViewById(R.id.finish_service)
        val serviceName: TextView = findViewById(R.id.serviceName_txt)
        uid = FirebaseAuth.getInstance().currentUser!!.uid

        serviceId = intent.extras?.get("serviceId").toString()
        mapFragment = supportFragmentManager
            .findFragmentById(R.id.my_map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        locationRequest = uiHelper.getLocationRequest()
        mapFragment.view?.visibility = View.GONE
        serviceName.text = intent.extras?.get("serviceName").toString()

        var driverName = "-"
        val uid = FirebaseAuth.getInstance().currentUser!!.uid
        rootRef.child(String.format("Users/%s", uid)).addListenerForSingleValueEvent(object:
            ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                driverName = dataSnapshot.child("name").value.toString()
            }})
        startService.setOnClickListener {
            startService()
            participantsPage.setMarker(markerList)
        }
        finishService.setOnClickListener {
            finishService()
        }
        shareButton.setOnClickListener {
            val myIntent = Intent(Intent.ACTION_SEND)
            myIntent.type = "text/plain"
            val shareBody = "The id required to register for the service named ${intent.extras?.get("serviceName").toString()} whose driver is $driverName :\n ${intent.extras?.get("serviceId").toString()}"
            myIntent.putExtra(Intent.EXTRA_TEXT,shareBody)
            startActivity(Intent.createChooser(myIntent,"Share Via"))
        }

        val adapter = ViewPagerAdapter(supportFragmentManager)
        participantsPage = ParticipantListPage(intent.extras?.get("serviceId").toString())
        adapter.addFragment(participantsPage, "Participants")
        adapter.addFragment(WaitingParticipantListPage(intent.extras?.get("serviceId").toString()), "Waiting Requests")
        //Adapter'ımızdaki verileri viewPager adapter'a veriyoruz
        val viewPager: ViewPager = findViewById(R.id.viewPager)
        viewPager.adapter = adapter
        //Tablar arasında yani viewPager'lar arasında geçisi sağlıyoruz
        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(viewPager)
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show()
            }
            else {
                Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show()
            }
        }
    }
    private fun startService(){
        driverMarkerNum=0
        mapFragment = supportFragmentManager
            .findFragmentById(R.id.my_map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        setUpMap()
        getStudentIds(serviceId)
        getSchoolLocation(serviceId)
        mapFragment.view?.visibility = View.VISIBLE
        rootRef.child("Services").child(serviceId).child("drive").setValue("on")
        startUpdateLocation()
        //createLocationCallback()
    }
    private fun finishService(){
        mapFragment.view?.visibility = View.GONE
        val rootRef = FirebaseDatabase.getInstance().reference
        val uidRef = rootRef.child("Services").child(serviceId)
        val eventListener3: ValueEventListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val where = dataSnapshot.child("where").value.toString()
                if(where == "goSchool"){
                    uidRef.child("where").setValue("goHome")
                }
                else{
                    uidRef.child("where").setValue("goSchool")
                }
            }
        }
        uidRef.addListenerForSingleValueEvent(eventListener3)
        uidRef.child("drive").setValue("off")
        //getStudentIdsForAbsent(serviceId)
        stopUpdateLocation()
    }
    private fun startUpdateLocation(){
        mToastRunnable.run();
    }
    private fun stopUpdateLocation(){
        mHandler.removeCallbacks(mToastRunnable);
    }
    private val mToastRunnable: Runnable = object : Runnable {
        override fun run() {
            updateLocation()
            mHandler.postDelayed(this, 1000)
        }
    }
    private fun updateLocation() {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)
            return
        }
        mMap.isMyLocationEnabled = true
        fusedLocationProviderClient.lastLocation.addOnSuccessListener(this) { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {
                lastLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                val rootRef = FirebaseDatabase.getInstance().reference
                val uidRef = serviceId.let { rootRef.child("Services").child(it) }
                uidRef.child("latitude").setValue(location.latitude.toString())
                uidRef.child("longitude").setValue(location.longitude.toString())
            }
        }
    }
    override fun onDestroy() {
        super.onDestroy()
        finishService()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.mapType = GoogleMap.MAP_TYPE_TERRAIN
        mMap.uiSettings.isZoomControlsEnabled = true
        mMap.setOnMarkerClickListener(this)
    }
    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)
            return
        }

        mMap.isMyLocationEnabled = true
        System.out.println("BURDAAAA")
        fusedLocationProviderClient.lastLocation.addOnSuccessListener(this) { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {
                System.out.println("GELDİİİİİ")
                lastLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                val rootRef = FirebaseDatabase.getInstance().reference
                val uidRef = serviceId.let { rootRef.child("Services").child(it) }
                uidRef.child("latitude").setValue(location.latitude.toString())
                uidRef.child("longitude").setValue(location.longitude.toString())
                /*if(driverMarkerNum == 0)
                    placeMarkerOnMap(currentLatLng,"location")*/
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
            }
        }
    }
    private fun placeMarkerOnMap(id:String,location: LatLng,drawableName: String?,name:String) {
        driverMarkerNum++
        markerOptions = MarkerOptions().position(location).icon(BitmapDescriptorFactory.fromBitmap(resizeBitmap(drawableName,90,82))).title(name)
        val marker = mMap.addMarker(markerOptions)
        markerList.put(id,marker)
    }
    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
    }

    override fun onMarkerClick(p0: Marker?) = false


    private fun getStudentIds(serviceId : String){
        val studentIdList = ArrayList<String>()
        rootRef.child(String.format("Services/%s/participants", serviceId)).addListenerForSingleValueEvent(object:
            ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (snapshot in dataSnapshot.children) {
                    val studentId: String? = snapshot.getValue(String::class.java)
                    if (studentId != null) {
                        studentIdList.add(studentId)
                    }
                }
                getStudentLocation(studentIdList)
            }})
    }
    private fun getStudentLocation(studentIds : ArrayList<String>){
        for(id in studentIds){
            rootRef.child(String.format("Users/%s", id)).addListenerForSingleValueEvent(object:
               ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val id:String =  dataSnapshot.child("userId").value.toString();
                    val name:String =  dataSnapshot.child("name").value.toString();
                    val latitude = dataSnapshot.child("latitude").value.toString()
                    val longitude = dataSnapshot.child("longitude").value.toString()
                    val absent = dataSnapshot.child("absent").value.toString()
                    if(absent == "false") {
                        val studentLatLng = LatLng(latitude.toDouble(), longitude.toDouble())
                        placeMarkerOnMap(id,studentLatLng, "student_home",name)
                    }
                }
            })
        }
    }

    private fun getSchoolLocation(serviceId: String){
        rootRef.child(String.format("Services/%s", serviceId)).addListenerForSingleValueEvent(object:
            ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val name = dataSnapshot.child("schoolName").value.toString()
                val latitude = dataSnapshot.child("schoolLatitude").value.toString()
                val longitude = dataSnapshot.child("schoolLongitude").value.toString()
                if(latitude != "null" && longitude != "null") {
                    val schoolLatLng = LatLng(latitude.toDouble(), longitude.toDouble())
                    placeMarkerOnMap(serviceId, schoolLatLng, "school", name)
                }

            }
        })
    }

    private fun getStudentIdsForAbsent(serviceId : String){
        var studentIdList = ArrayList<String>()
        rootRef.child(String.format("Services/%s/participants", serviceId)).addListenerForSingleValueEvent(object:
            ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (snapshot in dataSnapshot.children) {
                    val studentId: String? = snapshot.getValue(String::class.java)
                    if (studentId != null) {
                        studentIdList.add(studentId)
                    }
                }
                changeStudentAbsent(studentIdList)
            }})
    }
    private fun changeStudentAbsent(studentIds : ArrayList<String>){
        for(id in studentIds){
            rootRef.child(String.format("Users/%s", id)).addListenerForSingleValueEvent(object:
                ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val absent = dataSnapshot.child("absent").value.toString()
                    if(absent == "true") {
                        rootRef.child("Users").child(id).child("absent").setValue("false")
                    }
                    rootRef.child("Users").child(id).child("status").setValue("Got Off")
                }
            })
        }
    }
}
