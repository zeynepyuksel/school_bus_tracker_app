package com.example.school_bus_tracker_app.ui.act_splash

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Handler
import android.util.Log
import com.example.school_bus_tracker_app.R
import com.example.school_bus_tracker_app.core.BaseActivity
import com.example.school_bus_tracker_app.databinding.SplashPageLayoutBinding
import com.example.school_bus_tracker_app.enum.UserType
import com.example.school_bus_tracker_app.ui.act_driver.DriverHomePage
import com.example.school_bus_tracker_app.ui.act_parent.ParentHomePage
import com.example.school_bus_tracker_app.ui.act_student.StudentHomePage
import com.example.school_bus_tracker_app.ui.act_user_management.UserManagement
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener


@Suppress("DEPRECATION")
class SplashPage : BaseActivity<SplashPageLayoutBinding>(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_page_layout)
        val sharedPreferences = this.getSharedPreferences(packageName, Context.MODE_PRIVATE)
        val id = sharedPreferences.getString("userId"," ")
        val isConnected = checkInternetConnection()
        if (!isConnected) {
            Snackbar.make(binding.root,"You don't have any internet connection. Please connect to the internet.",
                Snackbar.LENGTH_LONG
            )
            .addCallback(object : Snackbar.Callback() {
                override fun onDismissed(snackbar: Snackbar, event: Int) {
                    finish()
                }
            }).show()
        }
        else {
            Handler().postDelayed({
                if (id == " ") {
                    val intent = Intent(this, UserManagement::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    // Toast.makeText(baseContext,"id $id", Toast.LENGTH_LONG).show()
                    val rootRef = FirebaseDatabase.getInstance().reference
                    var uidRef = id.let { it?.let { it1 -> rootRef.child("Users").child(it1) } }
                    val eventListener: ValueEventListener = object : ValueEventListener {
                        override fun onCancelled(error: DatabaseError) {
                            TODO("Not yet implemented")
                        }

                        override fun onDataChange(dataSnapshot: DataSnapshot) {
                            val str: String = dataSnapshot.child("userType").value.toString()
                            if (str == UserType.Parent.name)
                                updateUI<ParentHomePage>()
                            if (str == UserType.Driver.name)
                                updateUI<DriverHomePage>()
                            if (str == UserType.Student.name)
                                updateUI<StudentHomePage>()
                        }
                    }
                    uidRef?.addListenerForSingleValueEvent(eventListener)
                }
            }, 1000)
        }
    }
    override fun getLayout() = R.layout.splash_page_layout

    fun checkInternetConnection(): Boolean {
        val cm =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        // test for connection
        return if (cm?.activeNetworkInfo != null && cm.activeNetworkInfo!!.isAvailable
            && cm.activeNetworkInfo!!.isConnected
        ) {
            true
        } else {
            Log.d("MyTest", "No Internet Connection")
            false
        }
    }

}