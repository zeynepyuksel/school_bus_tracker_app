package com.example.school_bus_tracker_app.ui.act_driver.adapter
import android.graphics.Color
import android.widget.Button
import android.widget.TextView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.example.school_bus_tracker_app.R
import com.example.school_bus_tracker_app.model.StudentModel
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class ParticipantAdapter (data: MutableList<StudentModel>,a : String) : BaseQuickAdapter<StudentModel, BaseViewHolder>(R.layout.participant_card_view_layout){
    val serviceId = a
    private val rootRef = FirebaseDatabase.getInstance().reference
    private var seeConnectorsClickListener: ParticipantAdapter.ItemClickListener? = null
    private var seeConnectorsGetOnClickListener: ParticipantAdapter.ItemClickListener? = null
    private var seeConnectorsGetOffClickListener: ParticipantAdapter.ItemClickListener? = null

    override fun convert(holder: BaseViewHolder, item: StudentModel) {
        holder.setText(R.id.txtParticipantName,item.name)
        holder.setText(R.id.txtParticipantSchoolName,item.schoolName)
        val txtParticipantName : TextView = holder.getView(R.id.txtParticipantName)
        val txtParticipantSchoolName : TextView = holder.getView(R.id.txtParticipantSchoolName)
        if(item.absent == "true"){
            txtParticipantName.setTextColor(Color.parseColor("#FF0000"))
            txtParticipantSchoolName.setTextColor(Color.parseColor("#FF0000"))
        }
        else{
            txtParticipantName.setTextColor(Color.parseColor("#0D0E0E"))
            txtParticipantSchoolName.setTextColor(Color.parseColor("#0D0E0E"))
        }
        holder.getView<Button>(R.id.participant_delete_btn).setOnClickListener {
            seeConnectorsClickListener?.onClickSeeConnectors(item)
        }
        holder.getView<Button>(R.id.got_on_button).setOnClickListener {
            seeConnectorsGetOnClickListener?.onClickSeeConnectors(item)
        }
        holder.getView<Button>(R.id.got_off_button).setOnClickListener {
            seeConnectorsGetOffClickListener?.onClickSeeConnectors(item)
        }
    }
    public fun editList(item : StudentModel){
        val position = data.indexOf(item)
        data.removeAt(position)
        notifyDataSetChanged()
        rootRef.child("Services").child(serviceId).child("participants").removeValue()
        reduceWaitingNum(true)
        for(value in data){
            rootRef.child("Services").child(serviceId).child("participants").child((data.indexOf(value)).toString()).setValue(value.userId)
        }
        reduceWaitingNum(false)
    }
    fun setOnSeeConnectorsClickListener(listener: ItemClickListener?) {
        seeConnectorsClickListener = listener
    }
    fun setOnSeeConnectorsGetOnClickListener(listener: ItemClickListener?) {
        seeConnectorsGetOnClickListener = listener
    }
    fun setOnSeeConnectorsGetOffClickListener(listener: ItemClickListener?) {
        seeConnectorsGetOffClickListener = listener
    }
    interface ItemClickListener {
        fun onClickSeeConnectors(student : StudentModel)
    }
    private fun reduceWaitingNum(zeroOrNot : Boolean){
        val uidRef = serviceId.let { rootRef.child("Services").child(it) }
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val waitingNumberStr = dataSnapshot.child("participantNumber").value.toString()
                var waitingNumber = waitingNumberStr.toInt()
                if(zeroOrNot)
                    waitingNumber = 0
                else
                    waitingNumber = data.size
                uidRef.child("participantNumber").setValue(waitingNumber.toString())
            }}
        uidRef.addListenerForSingleValueEvent(eventListener)
    }
}