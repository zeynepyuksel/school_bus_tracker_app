package com.example.school_bus_tracker_app.ui.act_student.profilePage

import android.view.View
import android.os.Bundle
import com.example.school_bus_tracker_app.R
import com.example.school_bus_tracker_app.core.BaseActivity
import com.example.school_bus_tracker_app.databinding.StudentProfilePageLayoutBinding
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.auth.FirebaseAuth


class StudentProfilePage : BaseActivity<StudentProfilePageLayoutBinding>() {
    private val rootRef = FirebaseDatabase.getInstance().reference
    private var uid = FirebaseAuth.getInstance().currentUser?.uid

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.student_profile_page_layout)
        showProfieInfo()
        binding.editPageBtn.setOnClickListener{
            updateUI2<StudentEditProfilePage>()
        }
        binding.backPageBtn.setOnClickListener {
            onBackPressed()
        }
    }
    override fun getLayout() = R.layout.student_profile_page_layout
    private fun showProfieInfo(){
        var uidRef = uid?.let { rootRef.child("Users").child(it) }
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                }
                else{
                    val name = dataSnapshot.child("name").value.toString()
                    val age = dataSnapshot.child("age").value.toString()
                    val telephoneNumber = dataSnapshot.child("telephoneNumber").value.toString()
                    val schoolName = dataSnapshot.child("schoolName").value.toString()
                    binding.studentNameSurname.text = name
                    binding.studentAge.text = age
                    binding.studentTelephoneNumber.text = telephoneNumber
                    binding.studentSchoolName.text = schoolName
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {}
        }
        uidRef?.addListenerForSingleValueEvent(eventListener)

    }
}