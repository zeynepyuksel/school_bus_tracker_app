package com.example.school_bus_tracker_app.model

open class User{
    var email: String = ""
    var age: String = ""
    var telephoneNumber: String = ""
    var password: String = ""
    var name: String = ""
    var imageUrl: String = ""
    var latitude : String = ""
    var longitude : String = ""
    var userType : String = ""
    var userId : String = ""
    var notificationNumber : String =""
    var schoolLatitude : String =""
    var schoolLongitude : String =""
    fun init(userTypeInput:String, nameInput:String, emailInput:String, ageInput : String, telephoneNumberInput : String, passwordInput : String,notificationNumberInput : String) {
        userType = userTypeInput
        name = nameInput
        email = emailInput
        age = ageInput
        telephoneNumber = telephoneNumberInput
        password = passwordInput
        notificationNumber = notificationNumberInput
    }

}
