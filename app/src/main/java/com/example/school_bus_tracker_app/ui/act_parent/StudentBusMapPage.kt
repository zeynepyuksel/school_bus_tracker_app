package com.example.school_bus_tracker_app.ui.act_parent

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager.PERMISSION_DENIED
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.graphics.Color
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import com.example.school_bus_tracker_app.R
import com.example.school_bus_tracker_app.core.BaseActivity
import com.example.school_bus_tracker_app.databinding.ParentMapLayoutBinding
import com.example.school_bus_tracker_app.notification.NotificationPage
import com.example.school_bus_tracker_app.ui.act_map.GoogleMapHelper
import com.example.school_bus_tracker_app.ui.act_map.LatLngInterpolator
import com.example.school_bus_tracker_app.ui.act_map.MarkerAnimationHelper
import com.example.school_bus_tracker_app.ui.act_map.UiHelper
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.base_alert_dialog_layout.view.*

@Suppress("DEPRECATION")
class StudentBusMapPage : BaseActivity<ParentMapLayoutBinding>(), OnMapReadyCallback,GoogleMap.OnMarkerClickListener {
    private lateinit var locationCallback: LocationCallback
    private var currentPositionMarker: Marker? = null
    private val googleMapHelper = GoogleMapHelper()
    private val markerAnimationHelper = MarkerAnimationHelper()
    private lateinit var locationRequest: LocationRequest
    private val uiHelper = UiHelper()
    private var locationFlag = true
    private var homeLocationFlag = true
    private var schoolLocationFlag = true
    lateinit var serviceId : String
    lateinit var studentId: String
    private var drive = "off"
    lateinit var studentName: String
    lateinit var serviceName : TextView
    lateinit var driverName : TextView
    lateinit var notifyDriver:Button
    lateinit var callDriver:Button
    lateinit var cameraHome : ImageButton
    lateinit var cameraBus : ImageButton
    lateinit var cameraSchool : ImageButton
    lateinit var driverId: String
    private val mHandler: Handler = Handler()
    private lateinit var googleMap: GoogleMap
    private lateinit var locationProviderClient: FusedLocationProviderClient
    private var uid : String? = null
    private lateinit var lastLocation: Location
    private lateinit var latLng : LatLng
    private lateinit var schoolLatLng : LatLng
    private lateinit var busLatLng : LatLng
    private lateinit var markerOptions : MarkerOptions
    private lateinit var zoom : ZoomControls

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.parent_map_layout)
        uid = FirebaseAuth.getInstance().currentUser?.uid
        serviceName = findViewById(R.id.service_txt_parent)
        driverName = findViewById(R.id.driver_name_parent)
        notifyDriver = findViewById(R.id.notifyDriver)
        callDriver = findViewById(R.id.driverCall)
        cameraHome = findViewById(R.id.cameraHome)
        cameraBus = findViewById(R.id.cameraBus)
        cameraSchool = findViewById(R.id.cameraSchool)
        zoom = findViewById(R.id.zoom)
        serviceId = intent.extras?.get("serviceId").toString()
        studentId = intent.extras?.get("studentId").toString()
        studentName = intent.extras?.get("studentName").toString()
        val lat = intent.extras?.get("Lat").toString()
        val long = intent.extras?.get("Long").toString()
        latLng = LatLng(lat.toDouble(), long.toDouble())
        val schoolLat = intent.extras?.get("schoolLatitude").toString()
        val schoolLong = intent.extras?.get("schoolLongitude").toString()
        schoolLatLng = LatLng(schoolLat.toDouble(), schoolLong.toDouble())

        serviceInfo()
        editDriverInfo()
        callDriver.setOnClickListener {
            callDriverr()
        }
        notifyDriver.setOnClickListener {
            createAlertDialog()
        }
        changeStudentWontComeTextColor()
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.student_map_parent) as SupportMapFragment
        mapFragment.getMapAsync { googleMap = it }
        locationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        zoom.setOnZoomOutClickListener { googleMap.animateCamera(CameraUpdateFactory.zoomOut()) }
        zoom.setOnZoomInClickListener { googleMap.animateCamera(CameraUpdateFactory.zoomIn()) }
        createLocationCallback()
        locationRequest = uiHelper.getLocationRequest()
        if (!uiHelper.isPlayServicesAvailable(this)) {
            Toast.makeText(this, "Play Services did not installed!", Toast.LENGTH_SHORT).show()
            finish()
        } else requestLocationUpdate()
        cameraHome.setOnClickListener {animateCamera(latLng)}
        cameraBus.setOnClickListener {
            Log.d("*/*/*/*/",drive)
            if(drive == "on")
                animateCamera(busLatLng)
        }
        cameraSchool.setOnClickListener {animateCamera(schoolLatLng)}
    }
    override fun getLayout() = R.layout.parent_map_layout
    private fun changeStudentWontComeTextColor(){
        val rootRef = FirebaseDatabase.getInstance().reference
        val uidRef = studentId.let { rootRef.child("Users").child(it) }
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                    Toast.makeText(
                        this@StudentBusMapPage,
                        getString(R.string.student_not_found),
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    val absent = dataSnapshot.child("absent").value.toString()
                    if(absent == "true"){
                        notifyDriver.setTextColor(Color.parseColor("#FF0000"))
                    }
                    else{
                        notifyDriver.setTextColor(Color.parseColor("#0D0E0E"))
                    }
                }
            }
        }
        uidRef.addListenerForSingleValueEvent(eventListener)
    }
    private fun changeStudentAbsent(){
        val rootRef = FirebaseDatabase.getInstance().reference
        val uidRef = studentId.let { rootRef.child("Users").child(it) }
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                    Toast.makeText(
                        this@StudentBusMapPage,
                        getString(R.string.student_not_found),
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    val absent = dataSnapshot.child("absent").value.toString()
                    if(absent == "true"){
                        uidRef.child("absent").setValue("false")
                        notifyDriver.setTextColor(Color.parseColor("#0D0E0E"))
                    }
                    else{
                        uidRef.child("absent").setValue("true")
                        notifyDriver.setTextColor(Color.parseColor("#FF0000"))
                        val a = NotificationPage()
                        uid?.let { it1 ->
                            a.createNotification(
                                it1,
                                driverId,
                                "$studentName won't come to the service named ${serviceName.text}.",
                                serviceId
                            )
                        }
                    }

                }
            }
        }
        uidRef.addListenerForSingleValueEvent(eventListener)
    }
    private fun callDriverr(){
        val rootRef = FirebaseDatabase.getInstance().reference
        val uidRef = serviceId.let { rootRef.child("Services").child(it) }
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                    Toast.makeText(this@StudentBusMapPage,getString(R.string.service_not_found), Toast.LENGTH_LONG).show()
                } else {
                    val driverId = dataSnapshot.child("driverId").value.toString()
                    takeDriverNumber(driverId)
                }
            }
        }
        uidRef.addListenerForSingleValueEvent(eventListener)
    }
    private fun takeDriverNumber(driverID:String){
        val rootRef = FirebaseDatabase.getInstance().reference
        val uidRef = driverID.let { rootRef.child("Users").child(it) }
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                    Toast.makeText(this@StudentBusMapPage,getString(R.string.not_found_driver), Toast.LENGTH_LONG).show()
                } else {
                    val telNumber = dataSnapshot.child("telephoneNumber").value.toString()
                    val intent =
                        Intent(Intent.ACTION_DIAL, Uri.parse("tel:$telNumber"))
                    startActivity(intent)
                }
            }
        }
        uidRef.addListenerForSingleValueEvent(eventListener)
    }
    private fun serviceInfo(){
        val rootRef = FirebaseDatabase.getInstance().reference
        val uidRef = serviceId.let { rootRef.child("Services").child(it) }
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                    Toast.makeText(this@StudentBusMapPage,getString(R.string.service_not_found), Toast.LENGTH_LONG).show()
                } else {
                    val serviceNameTxt = dataSnapshot.child("serviceName").value.toString()
                    serviceName.text = serviceNameTxt
                }
            }
        }
        uidRef.addListenerForSingleValueEvent(eventListener)
    }
    private fun editDriverInfo(){
        val rootRef = FirebaseDatabase.getInstance().reference
        val uidRef = serviceId.let { rootRef.child("Services").child(it) }
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                    Toast.makeText(this@StudentBusMapPage,getString(R.string.not_found_driver),Toast.LENGTH_LONG).show()
                }
                else{
                    driverId = dataSnapshot.child("driverId").value.toString()
                    val uidRef2 =  rootRef.child("Users").child(driverId)
                    val eventListener2 : ValueEventListener = object : ValueEventListener{
                        @SuppressLint("SetTextI18n")
                        override fun onDataChange(snapshot: DataSnapshot) {
                            val name:String = snapshot.child("name").value.toString()
                            val age:String = snapshot.child("age").value.toString()
                            driverName.text = "$name($age)"
                        }
                        override fun onCancelled(error: DatabaseError) {
                        }
                    }
                    uidRef2.addListenerForSingleValueEvent(eventListener2)
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {}
        }
        uidRef.addListenerForSingleValueEvent(eventListener)
    }
    private fun createAlertDialog(){
        val mDialogView = layoutInflater.inflate(R.layout.base_alert_dialog_layout,null)
        mDialogView.message.text = getString(R.string.are_you_sure)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
            .setCancelable(false)
        val mAlertDialog = mBuilder?.show()
        mDialogView.alert_okey.setOnClickListener {
            changeStudentAbsent()
            mAlertDialog?.dismiss()
        }
        mDialogView.alert_cancel.setOnClickListener {
            mAlertDialog?.dismiss()
        }
        mAlertDialog?.window?.setBackgroundDrawableResource(R.drawable.round_corner)
    }
    override fun onMapReady(p0: GoogleMap) {
        googleMap = p0
        googleMap.mapType = GoogleMap.MAP_TYPE_TERRAIN
        googleMap.uiSettings.isZoomControlsEnabled = true
        googleMap.uiSettings.isCompassEnabled = true
        googleMap.uiSettings.isMyLocationButtonEnabled = true
        googleMap.setOnMarkerClickListener(this)
    }

    override fun onMarkerClick(p0: Marker?) = false
    private fun createLocationCallback() {
        locationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            super.onLocationResult(locationResult)
            if (locationResult!!.lastLocation == null) return
            val u =
                LatLng(locationResult.lastLocation.latitude, locationResult.lastLocation.longitude)
            val rootRef = FirebaseDatabase.getInstance().reference
            val uidRef = serviceId.let { rootRef.child("Services").child(it) }
            val eventListener: ValueEventListener = object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    if (!dataSnapshot.exists()) {
                        Toast.makeText(
                            this@StudentBusMapPage,
                            getString(R.string.service_not_found),
                            Toast.LENGTH_LONG
                        ).show()
                    } else {
                        if(homeLocationFlag){
                            animateCamera(latLng)
                            showOrAnimateMarker(latLng,0)
                            homeLocationFlag = false
                        }
                        if(schoolLocationFlag){
                            //animateCamera(schoolLatLng)
                            showOrAnimateMarker(schoolLatLng,3)
                            schoolLocationFlag = false
                        }
                        drive = dataSnapshot.child("drive").value.toString()
                        val latitude = dataSnapshot.child("latitude").value.toString()
                        val longitude = dataSnapshot.child("longitude").value.toString()
                        if (drive == "on") {
                            showOrAnimateMarker(latLng,0)
                            busLatLng = LatLng(latitude.toDouble(), longitude.toDouble())
                            Log.e("Location", busLatLng.latitude.toString() + " , " + busLatLng.longitude)
                            if (locationFlag && drive=="on") {
                                locationFlag = false
                                animateCamera(busLatLng)
                            }
                            showOrAnimateMarker(busLatLng,1)
                        }
                        else{
                            if(currentPositionMarker != null){
                                currentPositionMarker!!.isVisible = false
                                currentPositionMarker = null
                                locationFlag = true
                                animateCamera(latLng)
                            }
                            showOrAnimateMarker(latLng,0)
                        }
                    }
                }
            }
            uidRef.addListenerForSingleValueEvent(eventListener)
        }}

    }
    /*private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
            )
            return
        }

        locationProviderClient.lastLocation.addOnSuccessListener(this) { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {
                lastLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                val rootRef = FirebaseDatabase.getInstance().reference
                val uidRef = serviceId.let { rootRef.child("Services").child(it) }
                uidRef.child("latitude").setValue(location.latitude.toString())
                uidRef.child("longitude").setValue(location.longitude.toString())
                /*if(driverMarkerNum == 0)
                    placeMarkerOnMap(currentLatLng,"location")*/
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
            }
        }
    }*/
    private fun addHome(){
        val rootRef = FirebaseDatabase.getInstance().reference
        val uidRef = studentId.let { rootRef.child("Users").child(it) }
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                    Toast.makeText(
                        this@StudentBusMapPage,
                        getString(R.string.student_not_found),
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    val latitude = dataSnapshot.child("latitude").value.toString()
                    val longitude = dataSnapshot.child("longitude").value.toString()
                    latLng = LatLng(latitude.toDouble(), longitude.toDouble())

                    }
                }
            }
        uidRef.addListenerForSingleValueEvent(eventListener)
    }
    private fun placeMarkerOnMap(location: LatLng,drawableName: Int,name:String) {
        // 1
        markerOptions  = MarkerOptions()
            .icon(BitmapDescriptorFactory.fromResource(drawableName))
            .position(location).title(name)
        // 2
        googleMap.addMarker(markerOptions)
        }
    private fun showOrAnimateMarker(latLng: LatLng,count:Int) {
        if (currentPositionMarker == null && count == 1)
            currentPositionMarker = googleMap.addMarker(googleMapHelper.getDriverMarkerOptions(R.drawable.bus,latLng))
        else if(count == 0){
            googleMap.addMarker(googleMapHelper.getDriverMarkerOptions(R.drawable.house,latLng))
        }
        else if(count == 3){
            googleMap.addMarker(googleMapHelper.getDriverMarkerOptions(R.drawable.school2,schoolLatLng))
        }
        else markerAnimationHelper.animateMarkerToGB(currentPositionMarker!!, latLng, LatLngInterpolator.Spherical())
    }
    private fun animateCamera(latLng: LatLng) {
        val cameraUpdate = googleMapHelper.buildCameraUpdate(latLng)
        //googleMap.animateCamera(cameraUpdate, 10, null)
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12f))

    }
    @SuppressLint("MissingPermission")
    private fun requestLocationUpdate() {
        if (!uiHelper.isHaveLocationPermission(this)) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)
            return
        }
        locationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper())
    }
    companion object {
        private const val MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 2200
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION) {
            val value = grantResults[0]
            if (value == PERMISSION_DENIED) {
                Toast.makeText(this, "Location Permission denied", Toast.LENGTH_SHORT).show()
                finish()
            } else if (value == PERMISSION_GRANTED) requestLocationUpdate()
        }
    }
}