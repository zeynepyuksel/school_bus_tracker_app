package com.example.school_bus_tracker_app.ui.act_driver

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.school_bus_tracker_app.R
import com.example.school_bus_tracker_app.core.BaseActivity
import com.example.school_bus_tracker_app.databinding.ServiceListRecyclerviewLayoutBinding
import com.example.school_bus_tracker_app.model.ServiceModel
import com.example.school_bus_tracker_app.ui.act_driver.adapter.ServiceAdapter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import android.content.Intent
import android.util.Log
import androidx.appcompat.app.AlertDialog
import com.example.school_bus_tracker_app.ui.act_student.StudentHomePage
import kotlinx.android.synthetic.main.base_alert_dialog_layout.view.*
import kotlinx.android.synthetic.main.create_service_dialog_layout.view.service_name
import kotlinx.android.synthetic.main.create_service_dialog_layout.view.service_school_name
import kotlinx.android.synthetic.main.driver_service_edit_page_layout.view.*

class DriverServiceListPage : BaseActivity<ServiceListRecyclerviewLayoutBinding>() {
    val serviceList : MutableList<ServiceModel> = mutableListOf()
    private val serviceAdapter = ServiceAdapter(mutableListOf())
    private var onClickSeeConnectors: ServiceAdapter.ItemClickListener? =
        object : ServiceAdapter.ItemClickListener {
            override fun onClickSeeConnectors(service: ServiceModel) {
                serviceUpdateDialog(service)
            }
        }
    private var onClickSeeConnectorsDeleteService: ServiceAdapter.ItemClickDeleteListener? =
        object : ServiceAdapter.ItemClickDeleteListener {
            override fun onClickSeeConnectors(service: ServiceModel) {
                createAlertDialog(service)
            }
        }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.service_list_recyclerview_layout)
        binding.recylerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        serviceAdapter.setOnSeeConnectorsClickListener(onClickSeeConnectors)
        serviceAdapter.setOnSeeConnectorsClickDeleteListener(onClickSeeConnectorsDeleteService)
        binding.recylerView.adapter = serviceAdapter
        serviceAdapter.setOnItemClickListener { _, _, position ->
            val intent = Intent(baseContext, ServiceDetailPage::class.java)
            intent.putExtra("serviceName",serviceList[position].serviceName)
            intent.putExtra("serviceId",serviceList[position].serviceId)
            startActivity(intent)
        }
        getServiceIds()
    }
    override fun getLayout() = R.layout.service_list_recyclerview_layout
    private fun getServiceIds() {
        val servicesIdList = ArrayList<String>()
        val uid = FirebaseAuth.getInstance().currentUser!!.uid
        val rootRef = FirebaseDatabase.getInstance().reference
        rootRef.child(String.format("Users/%s/Services", uid)).addListenerForSingleValueEvent(object:
            ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (snapshot in dataSnapshot.children) {
                    val serviceId: String? = snapshot.getValue(String::class.java)
                    if (serviceId != null) {
                        servicesIdList.add(serviceId)
                    }
                }
                getServiceObjects(servicesIdList)
            }})
    }
    fun getServiceObjects(servicesIds : ArrayList<String>){
        val rootRef = FirebaseDatabase.getInstance().reference
        for(id in servicesIds){
            val service = ServiceModel()
            rootRef.child(String.format("Services/%s", id)).addListenerForSingleValueEvent(object:
                ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    service.serviceId = dataSnapshot.child("serviceId").value.toString()
                    service.serviceName = dataSnapshot.child("serviceName").value.toString()
                    service.schoolName = dataSnapshot.child("schoolName").value.toString()
                    service.driverId = dataSnapshot.child("driverId").value.toString()
                    service.participantNumber = dataSnapshot.child("participantNumber").value.toString()
                    service.participantRequestNumber = dataSnapshot.child("participantRequestNumber").value.toString()
                    service.schoolAddress = dataSnapshot.child("schoolAddress").value.toString()
                    serviceList.add(service)
                    if(id == servicesIds[servicesIds.size -1]){
                        serviceAdapter.notifyDataSetChanged()
                        serviceAdapter.setOnSeeConnectorsClickListener(onClickSeeConnectors)
                        serviceAdapter.setNewInstance(serviceList)
                        serviceAdapter.notifyDataSetChanged()
                    }
                }
            })
        }
    }
    private fun serviceUpdateDialog(service : ServiceModel){
        val mDialogView = layoutInflater.inflate(R.layout.driver_service_edit_page_layout,null)
        mDialogView.service_name.editText?.setText(service.serviceName)
        mDialogView.service_school_name.editText?.setText(service.schoolName)
        mDialogView.service_school_address.editText?.setText(service.schoolAddress)

        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
            .setCancelable(false)
        val mAlertDialog = mBuilder.show()
        mDialogView.save_btn.setOnClickListener {
            val serviceName = mDialogView.service_name.editText?.text.toString().trim()
            val schoolName = mDialogView.service_school_name.editText?.text.toString().trim()
            val schoolAddress = mDialogView.service_school_address.editText?.text.toString().trim()

            if (serviceName.isEmpty() or schoolName.isEmpty()) {
                if(serviceName.isEmpty())
                    mDialogView.service_name.error = getString(R.string.field_cant_empty)
                if(schoolName.isEmpty())
                    mDialogView.service_school_name.error = getString(R.string.field_cant_empty)
                if(schoolAddress.isEmpty())
                mDialogView.service_school_address.error = getString(R.string.field_cant_empty)
            }
            else {
                mDialogView.service_name.error = null
                mDialogView.service_school_name.error = null
                mDialogView.service_school_address.error = null
                service.serviceName = serviceName
                service.schoolName = schoolName
                service.schoolAddress = schoolAddress
                serviceAdapter.notifyDataSetChanged()
                mDialogView.service_name.editText?.setText(serviceName)
                mDialogView.service_school_name.editText?.setText(schoolName)
                mDialogView.service_school_address.editText?.setText(schoolAddress)
                update(serviceName,schoolName,schoolAddress,service)
                mAlertDialog.dismiss()
            }
        }
        mDialogView.cancel_btn.setOnClickListener {
            mAlertDialog.dismiss()
        }
        mAlertDialog.window?.setBackgroundDrawableResource(R.drawable.round_corner)
    }
    private fun update(service_name : String, school_name : String, school_address : String, service: ServiceModel){
        val rootRef = FirebaseDatabase.getInstance().reference
        val uidRef = rootRef.child("Services").child(service.serviceId)
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                uidRef.child("serviceName").setValue(service_name)
                uidRef.child("schoolName").setValue(school_name)
                uidRef.child("schoolAddress").setValue(school_address)

            }}
        uidRef.addListenerForSingleValueEvent(eventListener)
    }
    fun createAlertDialog(service:ServiceModel){
        val mDialogView = layoutInflater.inflate(R.layout.base_alert_dialog_layout,null)
        mDialogView.message.text = getString(R.string.deleteServiceMessage)
        val mBuilder = AlertDialog.Builder(this)
                .setView(mDialogView)
                .setCancelable(false)
        val mAlertDialog = mBuilder?.show()
        mDialogView.alert_okey.setOnClickListener{
            getStudentIds(service)
            mAlertDialog?.dismiss()
        }
        mDialogView.alert_cancel.setOnClickListener {
            mAlertDialog?.dismiss()
        }
        mAlertDialog?.window?.setBackgroundDrawableResource(R.drawable.round_corner)
    }
    private fun deleteStudentsFromService(studentList : ArrayList<String>,service :ServiceModel){
        val rootRef = FirebaseDatabase.getInstance().reference
        for(id in studentList){
            val uidRef = rootRef.child("Users").child(id)
            uidRef.child("myService").setValue(" ")
        }
        serviceAdapter.deleteService(service)
    }
    private fun getStudentIds(service: ServiceModel) {
        val studentIdList = ArrayList<String>()
        val uid = service.serviceId
        val rootRef = FirebaseDatabase.getInstance().reference
        rootRef.child(String.format("Services/%s/participants", uid)).addListenerForSingleValueEvent(object:
            ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (snapshot in dataSnapshot.children) {
                    val studentId: String? = snapshot.getValue(String::class.java)
                    if (studentId != null) {
                        studentIdList.add(studentId)
                    }
                }
                Log.d("Çocuklaar", studentIdList.toString())
                deleteStudentsFromService(studentIdList,service)
            }})
    }
}