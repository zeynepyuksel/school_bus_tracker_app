package com.example.school_bus_tracker_app.ui.act_parent

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.school_bus_tracker_app.R
import com.example.school_bus_tracker_app.core.BaseActivity
import com.example.school_bus_tracker_app.databinding.ParentHomePageLayoutBinding
import com.example.school_bus_tracker_app.notification.MyFirebaseMessagingService
import com.example.school_bus_tracker_app.notification.NotificationPage
import com.example.school_bus_tracker_app.ui.act_profile_page.ProfilePage

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.add_child_layout.view.*


class ParentHomePage : BaseActivity<ParentHomePageLayoutBinding>(){
    private var uid : String = " "
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.parent_home_page_layout)
        uid = FirebaseAuth.getInstance().currentUser!!.uid
        controlNotification(uid)

        binding.addChildBtn.setOnClickListener{
            addChildDialog()
        }
        binding.childrenBtn.setOnClickListener {
            controlChildrenList()
        }
        binding.userProfile.setOnClickListener {
            updateUI2<ProfilePage>()
        }
        binding.parentExitBtn.setOnClickListener {
            exit()
        }
        binding.notification.setOnClickListener{
            updateUI2<NotificationPage>()
        }
    }
    override fun getLayout() = R.layout.parent_home_page_layout
    private fun controlChildrenList(){
        val rootRef = FirebaseDatabase.getInstance().reference
        val uidRef = uid.let { rootRef.child("Users").child(it) }
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                    Toast.makeText(this@ParentHomePage,getString(R.string.child_not_found),Toast.LENGTH_LONG).show()
                }
                else{
                    val childNumberStr = dataSnapshot.child("childNumber").value.toString()
                    if(childNumberStr == "0"){
                        Toast.makeText(this@ParentHomePage,"You don't have children",Toast.LENGTH_LONG).show()
                    }
                    else{
                        updateUI2<ChildrenListPage>()
                    }
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {}
        }
        uidRef.addListenerForSingleValueEvent(eventListener)
    }
    private fun addChildDialog(){
        val mDialogView = layoutInflater.inflate(R.layout.add_child_layout,null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
            .setCancelable(false)

        val mAlertDialog = mBuilder.show()
        mDialogView.okey.setOnClickListener {
            val childID = mDialogView.child_id.editText?.text.toString().trim()
            if (childID.isEmpty()) {
                mDialogView.child_id.error = "Field can't be empty"

            } else {
                mDialogView.child_id.error = null
                controlParentNumber(childID)
                mAlertDialog.dismiss()
            }
        }
        mDialogView.cancel.setOnClickListener {
            mAlertDialog.dismiss()
        }
        mAlertDialog.window?.setBackgroundDrawableResource(R.drawable.round_corner)
    }
    private fun controlParentNumber(childId : String){
        val rootRef = FirebaseDatabase.getInstance().reference
        var uidRef = childId.let { rootRef.child("Users").child(it) }
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                    Toast.makeText(this@ParentHomePage,getString(R.string.child_not_found),Toast.LENGTH_LONG).show()
                }
                else{
                    val parentNumberStr = dataSnapshot.child("parentNumber").value.toString()
                    var parentNumber = parentNumberStr.toInt()
                    addChild(childId,parentNumber)
                }
                Toast.makeText(this@ParentHomePage,getString(R.string.child_added),Toast.LENGTH_LONG).show()
            }
            override fun onCancelled(databaseError: DatabaseError) {}
        }
        uidRef.addListenerForSingleValueEvent(eventListener)
    }
    private fun addChild(childId : String,num :Int){
        if(num < 2) {
            val rootRef = FirebaseDatabase.getInstance().reference
            var uidRef = uid.let { rootRef.child("Users").child(it) }
            val eventListener: ValueEventListener = object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    if (!dataSnapshot.exists()) {
                        Toast.makeText(
                            this@ParentHomePage,
                            getString(R.string.child_not_found),
                            Toast.LENGTH_LONG
                        ).show()
                    } else {
                        val childNumberStr = dataSnapshot.child("childNumber").value.toString()
                        var childNumber = childNumberStr.toInt()
                        uidRef = uidRef.child("Children")
                        uidRef.child(childNumber.toString()).setValue(childId)
                        childNumber += 1
                        uidRef = uid.let { rootRef.child("Users").child(it) }
                        uidRef.child("childNumber").setValue(childNumber.toString())
                        //çocugunun parentlerine bu parenti ekle
                        addParent(childId)
                        Toast.makeText(
                            this@ParentHomePage,
                            getString(R.string.child_added),
                            Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onCancelled(databaseError: DatabaseError) {}
            }
            uidRef.addListenerForSingleValueEvent(eventListener)
        }
        else{
            Toast.makeText(this@ParentHomePage,"Student has reached the maximum number of parents.",Toast.LENGTH_LONG).show()
        }
    }
    private fun addParent(childId: String){
        val rootRef = FirebaseDatabase.getInstance().reference
        var uidRef = childId.let { rootRef.child("Users").child(it) }
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                    Toast.makeText(this@ParentHomePage,getString(R.string.child_not_found),Toast.LENGTH_LONG).show()
                }
                else{
                    val parentNumberStr = dataSnapshot.child("parentNumber").value.toString()
                    var parentNumber = parentNumberStr.toInt()
                    uidRef = uidRef.child("Parents")
                    uidRef.child(parentNumber.toString()).setValue(uid)
                    parentNumber += 1
                    uidRef = childId.let { rootRef.child("Users").child(it) }
                    uidRef.child("parentNumber").setValue(parentNumber.toString())
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {}
        }
        uidRef.addListenerForSingleValueEvent(eventListener)
    }
    private fun controlNotification(userId:String){
        val rootRef = FirebaseDatabase.getInstance().reference
        println("uid : $userId")
        val uidRef = userId.let { rootRef.child("Users").child(it) } .child("Notifications")
        uidRef.keepSynced(true)
        uidRef.addChildEventListener(object : ChildEventListener {
            override fun onCancelled(error: DatabaseError) {
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
            }

            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                val str:String = snapshot.value.toString()
                println("str : $str uid $userId")
                val gone = "false"
                val c = FirebaseDatabase.getInstance().reference
                val uidReference2 = c.child("Notifications").child(str)
                val eventListener2: ValueEventListener = object : ValueEventListener {
                    override fun onCancelled(error: DatabaseError) {
                        TODO("Not yet implemented")
                    }
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        if (!dataSnapshot.exists()) {
                        } else {
                            val gone2 = dataSnapshot.child("gone").value.toString()
                            val from = dataSnapshot.child("fromName").value.toString()
                            val message = dataSnapshot.child("text").value.toString()
                            if(gone2 == gone){
                                val controlNotification = MyFirebaseMessagingService()
                                controlNotification.showNotification(applicationContext,from,message)
                                uidReference2.child("gone").setValue("true")
                            }
                        }
                    }
                }
                uidReference2.addListenerForSingleValueEvent(eventListener2)
            }
            override fun onChildRemoved(snapshot: DataSnapshot) {
            }
        })
    }

}