package com.example.school_bus_tracker_app.ui.act_student

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.school_bus_tracker_app.R
import com.example.school_bus_tracker_app.core.BaseActivity
import com.example.school_bus_tracker_app.databinding.StudentHomePageLayoutBinding
import com.example.school_bus_tracker_app.notification.MyFirebaseMessagingService
import com.example.school_bus_tracker_app.notification.NotificationPage
import com.example.school_bus_tracker_app.ui.act_student.profilePage.StudentProfilePage
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.add_child_layout.view.cancel
import kotlinx.android.synthetic.main.add_child_layout.view.okey
import kotlinx.android.synthetic.main.student_add_service.view.*

class StudentHomePage : BaseActivity<StudentHomePageLayoutBinding>() {
    private var uid : String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.student_home_page_layout)
        uid = FirebaseAuth.getInstance().currentUser!!.uid
        controlNotification(uid)
        binding.registerService.setOnClickListener {
            addService()
        }
        binding.studentId.setOnClickListener {
            getId()
        }
        binding.userProfile.setOnClickListener {
            updateUI2<StudentProfilePage>()
        }
        binding.myService.setOnClickListener {
            controlServiceId()
        }
        binding.notification.setOnClickListener {
            updateUI2<NotificationPage>()
        }
        binding.studentExit.setOnClickListener {
            exit()
        }
    }
    fun controlNotification(userId:String){
        val rootRef = FirebaseDatabase.getInstance().reference
        System.out.println("uid : "+userId)
        val uidRef = userId.let { rootRef.child("Users").child(it) } .child("Notifications")
        uidRef.keepSynced(true)
        uidRef.addChildEventListener(object : ChildEventListener {
            override fun onCancelled(error: DatabaseError) {
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
            }

            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                val str:String = snapshot.value.toString()
                System.out.println("str : "+str+" uid "+userId)
                val gone = "false"
                val c = FirebaseDatabase.getInstance().reference
                val uidReference2 = c.child("Notifications").child(str)
                val eventListener2: ValueEventListener = object : ValueEventListener {
                    override fun onCancelled(error: DatabaseError) {
                        TODO("Not yet implemented")
                    }
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        if (!dataSnapshot.exists()) {
                        } else {
                            val gone2 = dataSnapshot.child("gone").value.toString()
                            val from = dataSnapshot.child("fromName").value.toString()
                            val message = dataSnapshot.child("text").value.toString()
                            if(gone2 == gone){
                                val controlNotification = MyFirebaseMessagingService()
                                controlNotification.showNotification(applicationContext,from,message)
                                uidReference2.child("gone").setValue("true")
                            }
                        }
                    }
                }
                uidReference2.addListenerForSingleValueEvent(eventListener2)
            }
            override fun onChildRemoved(snapshot: DataSnapshot) {
            }
        })
    }
    private fun controlServiceId(){
        val rootRef = FirebaseDatabase.getInstance().reference
        val uidRef = uid.let { rootRef.child("Users").child(it) }
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                    Toast.makeText(this@StudentHomePage,getString(R.string.student_not_found), Toast.LENGTH_LONG).show()
                } else {
                    val serviceId = dataSnapshot.child("myService").value.toString()
                    if (serviceId != " "){
                        updateUI2<MyServicePage>()
                    }
                    else{
                        Toast.makeText(this@StudentHomePage,getString(R.string.you_dont_have_service),Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
        uidRef.addListenerForSingleValueEvent(eventListener)
    }
    override fun getLayout() = R.layout.student_home_page_layout
    private fun getId(){
        val uid = FirebaseAuth.getInstance().currentUser!!.uid
        val myIntent = Intent(Intent.ACTION_SEND)
        myIntent.type = "text/plain"
        val shareBody = uid
        myIntent.putExtra(Intent.EXTRA_TEXT,shareBody)
        startActivity(Intent.createChooser(myIntent,"Share Via"))
    }
    private fun addService(){
        val mDialogView = layoutInflater.inflate(R.layout.student_add_service,null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView)
            .setCancelable(false)

        val mAlertDialog = mBuilder.show()
        mDialogView.okey.setOnClickListener {
            val serviceId = mDialogView.service_id.editText?.text.toString().trim()
            val location = mDialogView.locationAddress.editText?.text.toString().trim()
            val schoolName = mDialogView.school_name.editText?.text.toString().trim()


            if (serviceId.isEmpty())
                mDialogView.service_id.error = getString(R.string.serviceId_cant_empty)
            if (location.isEmpty()) {
                mDialogView.locationAddress.error = getString(R.string.home_address_cant_empty)
            }
            if (schoolName.isEmpty()) {
                mDialogView.school_name.error = getString(R.string.school_name_cant_empty)
            }
            else {
                mDialogView.service_id.error = null
                mDialogView.locationAddress.error = null
                mDialogView.school_name.error = null
                registerService(serviceId,location,schoolName)
                mAlertDialog.dismiss()
            }
        }
        mDialogView.cancel.setOnClickListener {
            mAlertDialog.dismiss()
        }
        mAlertDialog.window?.setBackgroundDrawableResource(R.drawable.round_corner)
    }
    private fun registerService(serviceId : String,location:String,schoolName:String) {
        val rootRef = FirebaseDatabase.getInstance().reference
        var uidRef = serviceId.let { rootRef.child("Services").child(it) }
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                    Toast.makeText(this@StudentHomePage,getString(R.string.service_not_found), Toast.LENGTH_LONG).show()
                } else {
                    val waitingNumberStr = dataSnapshot.child("participantRequestNumber").value.toString()
                    var waitingNumber = waitingNumberStr.toInt()
                    uidRef.child("waitingParticipants").child(waitingNumber.toString()).setValue(uid)
                    waitingNumber += 1
                    uidRef = serviceId.let { rootRef.child("Services").child(it) }
                    uidRef.child("participantRequestNumber").setValue(waitingNumber.toString())
                }
                Toast.makeText(this@StudentHomePage,getString(R.string.request_send),Toast.LENGTH_LONG).show()
                }
        }
        uidRef.addListenerForSingleValueEvent(eventListener)

        var uidRef2 = serviceId.let { rootRef.child("Services").child(it) }
        val eventListener2: ValueEventListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (!dataSnapshot.exists()) {
                } else {
                    val serviceName = dataSnapshot.child("serviceName").value.toString()
                    val driverId = dataSnapshot.child("driverId").value.toString()
                    uid = FirebaseAuth.getInstance().currentUser!!.uid
                    val a = NotificationPage()
                    uid.let { it1 -> a.createNotification(it1,driverId,"There is a registration request for the service named $serviceName.",serviceId)}
                }
            }
        }
        uidRef2.addListenerForSingleValueEvent(eventListener2)
        //öğrencinin konum bilgisi database e yazılır.
        getLatLog(location,uid,"Users","latitude","longitude")
        val rootRef2 = FirebaseDatabase.getInstance().reference

        val uidRef3 = uid.let { rootRef2.child("Users").child(it) }
        uidRef3.child("homeAddress").setValue(location)
        uidRef3.child("schoolName").setValue(schoolName)
    }
}