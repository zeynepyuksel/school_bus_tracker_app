package com.example.school_bus_tracker_app.notification

import android.widget.Button
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.example.school_bus_tracker_app.R
import com.example.school_bus_tracker_app.model.NotificationModel
import com.example.school_bus_tracker_app.model.StudentModel
import com.example.school_bus_tracker_app.ui.act_driver.adapter.WaitingParticipantAdapter
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class NotificationAdapter (data: MutableList<NotificationModel>) : BaseQuickAdapter<NotificationModel, BaseViewHolder>(R.layout.notification_layout) {
    private var seeConnectorsDeleteClickListener: ItemClickNotificationDeleteListener? = null
    private val rootRef = FirebaseDatabase.getInstance().reference

    override fun convert(holder: BaseViewHolder, item: NotificationModel) {
        holder.setText(R.id.fromWho,item.fromName)
        holder.setText(R.id.message,item.text)
        holder.getView<Button>(R.id.delete_notification).setOnClickListener {
            seeConnectorsDeleteClickListener?.onClickSeeConnectors(item)
        }
    }
    //notification silme
    fun setOnSeeConnectorsDeleteClickListener(listener: ItemClickNotificationDeleteListener?) {
        seeConnectorsDeleteClickListener = listener
    }

    interface ItemClickNotificationDeleteListener {
        fun onClickSeeConnectors(notification : NotificationModel)
    }
    public fun editList(item : NotificationModel){
        val position = data.indexOf(item)
        data.removeAt(position)
        notifyDataSetChanged()
        rootRef.child("Users").child(item.to).child("Notifications").removeValue()
        reduceWaitingNum(true,item)
        for(value in data){
            rootRef.child("Users").child(item.to).child("Notifications").child((data.indexOf(value)).toString()).setValue(value.notificationId)
        }
        reduceWaitingNum(false,item)
        rootRef.child("Notifications").child(item.notificationId).removeValue()
    }

    private fun reduceWaitingNum(zeroOrNot : Boolean,notification: NotificationModel){
        val uidRef = notification.to.let { rootRef.child("Users").child(it)}
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val waitingNumberStr = dataSnapshot.child("notificationNumber").value.toString()
                var waitingNumber = waitingNumberStr.toInt()
                if(zeroOrNot)
                    waitingNumber = 0
                else
                    waitingNumber = data.size
                uidRef.child("notificationNumber").setValue(waitingNumber.toString())
            }}
        uidRef.addListenerForSingleValueEvent(eventListener)
    }
}