package com.example.school_bus_tracker_app.core

import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

abstract class DialogProviderActivity : AppCompatActivity() {

    protected var dialog: AlertDialog? = null
}