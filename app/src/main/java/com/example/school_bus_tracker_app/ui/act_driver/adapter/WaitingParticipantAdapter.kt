package com.example.school_bus_tracker_app.ui.act_driver.adapter

import android.widget.Button
import android.widget.TextView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.example.school_bus_tracker_app.R
import com.example.school_bus_tracker_app.model.StudentModel
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class WaitingParticipantAdapter(data: MutableList<StudentModel>,a : String) : BaseQuickAdapter<StudentModel, BaseViewHolder>(R.layout.waiting_participant_card_view_layout){
    val serviceId = a
    private val rootRef = FirebaseDatabase.getInstance().reference
    private var seeConnectorsClickListener: ItemClickListener? = null
    private var seeConnectorsDeleteClickListener: ItemClickDeleteListener? = null


    override fun convert(holder: BaseViewHolder, item: StudentModel) {
        holder.setText(R.id.txtWaitingParticipantName,item.name)
        holder.setText(R.id.txtWaitingParticipantSchoolName,item.schoolName)
        holder.getView<Button>(R.id.accept_btn).setOnClickListener {
            seeConnectorsClickListener?.onClickSeeConnectors(item)
            editList(item)
        }
        holder.getView<Button>(R.id.delete_btn).setOnClickListener {
            seeConnectorsDeleteClickListener?.onClickSeeConnectors(item)
            //editList(item)
        }
    }
    public fun editList(item : StudentModel){
        val position = data.indexOf(item)
        data.removeAt(position)
        notifyDataSetChanged()
        rootRef.child("Services").child(serviceId).child("waitingParticipants").removeValue()
        reduceWaitingNum(true)
        for(value in data){
            rootRef.child("Services").child(serviceId).child("waitingParticipants").child((data.indexOf(value)).toString()).setValue(value.userId)
        }
        reduceWaitingNum(false)
    }
    fun setOnSeeConnectorsClickListener(listener: ItemClickListener?) {
        seeConnectorsClickListener = listener
    }

    interface ItemClickListener {
        fun onClickSeeConnectors(student : StudentModel)
    }
    //talep silme
    fun setOnSeeConnectorsDeleteClickListener(listener: ItemClickDeleteListener?) {
        seeConnectorsDeleteClickListener = listener
    }

    interface ItemClickDeleteListener {
        fun onClickSeeConnectors(student : StudentModel)
    }
    private fun reduceWaitingNum(zeroOrNot : Boolean){
        val uidRef = serviceId.let { rootRef.child("Services").child(it) }
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val waitingNumberStr = dataSnapshot.child("participantRequestNumber").value.toString()
                var waitingNumber = waitingNumberStr.toInt()
                if(zeroOrNot)
                    waitingNumber = 0
                else
                    waitingNumber = data.size
                uidRef.child("participantRequestNumber").setValue(waitingNumber.toString())
            }}
        uidRef.addListenerForSingleValueEvent(eventListener)
    }

}