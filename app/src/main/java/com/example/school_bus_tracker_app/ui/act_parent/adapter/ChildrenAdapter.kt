package com.example.school_bus_tracker_app.ui.act_parent.adapter

import android.content.Intent
import android.widget.Button
import androidx.cardview.widget.CardView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.example.school_bus_tracker_app.R
import com.example.school_bus_tracker_app.model.StudentModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class ChildrenAdapter(data: MutableList<StudentModel>) : BaseQuickAdapter<StudentModel, BaseViewHolder>(R.layout.parent_child_card_view_item_layout) {
    private var seeConnectorsClickListener: ChildrenAdapter.ItemClickListener? = null
    private var seeConnectorsCardViewClickListener: ChildrenAdapter.ItemClickListener? = null

    private val rootRef = FirebaseDatabase.getInstance().reference
    private var uid = FirebaseAuth.getInstance().currentUser!!.uid

    override fun convert(holder: BaseViewHolder, item: StudentModel) {
        val name :String = item.name+"("+item.age+")"
        holder.setText(R.id.child_name,name)
        holder.setText(R.id.child_school_name,item.schoolName)
        holder.setText(R.id.child_status,item.status)
        holder.getView<Button>(R.id.participant_delete_btn).setOnClickListener {
            seeConnectorsClickListener?.onClickSeeConnectors(item)
        }
        holder.getView<CardView>(R.id.child_cardView).setOnClickListener{
            seeConnectorsCardViewClickListener?.onClickSeeConnectors(item)
        }
    }
    public fun editList(item : StudentModel){
        val position = data.indexOf(item)
        data.removeAt(position)
        notifyDataSetChanged()
        rootRef.child("Users").child(uid).child("Children").removeValue()
        reduceWaitingNum(true)
        for(value in data){
            rootRef.child("Users").child(uid).child("Children").child((data.indexOf(value)).toString()).setValue(value.userId)
        }
        reduceWaitingNum(false)
    }
    fun setOnSeeConnectorsClickListener(listener: ItemClickListener?) {
        seeConnectorsClickListener = listener
    }
    fun setOnSeeConnectorsCardViewClickListener(listener: ItemClickListener?) {
        seeConnectorsCardViewClickListener = listener
    }
    interface ItemClickListener {
        fun onClickSeeConnectors(student : StudentModel)
    }
    private fun reduceWaitingNum(zeroOrNot : Boolean){
        val uidRef = uid.let { rootRef.child("Users").child(it) }
        val eventListener: ValueEventListener = object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val childNumberStr = dataSnapshot.child("childNumber").value.toString()
                var childNumber = childNumberStr.toInt()
                if(zeroOrNot)
                    childNumber = 0
                else
                    childNumber = data.size
                uidRef.child("childNumber").setValue(childNumber.toString())
            }}
        uidRef.addListenerForSingleValueEvent(eventListener)
    }
}